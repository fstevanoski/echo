

import UIKit
import UserNotifications
import AVFoundation
import AudioToolbox
import Social
import FacebookCore
import TwitterKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var player : AVAudioPlayer!
    let nav1 = UINavigationController()
    let mainView = LandingViewController()
    let nav = UINavigationController()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        Twitter.sharedInstance().start(withConsumerKey:"9HXRKN6OsaNa6UzqLDLbB0YkP", consumerSecret:"aAXe628JvPkJ5ccZVtDCfeuQjLWs9mWF4ChQ8l8FzNJbGKNopE")
        
        let center = UNUserNotificationCenter.current()
        
        center.requestAuthorization(options: [.alert, .sound,.badge]) { (granted, error) in
            // actions based on whether notifications were authorized or not
            if granted {
                application.registerForRemoteNotifications()
            }
        }
        application.applicationIconBadgeNumber = 0
        let test = UserDefaults.standard.integer(forKey: "activationFinish")
        if test == 0 {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let navActivation = UINavigationController()
            let activation = ViewController()
            navActivation.pushViewController(activation, animated: true)
            self.window!.rootViewController = navActivation
            self.window?.makeKeyAndVisible()
        } else if test == 1 {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            nav1.pushViewController(mainView, animated: true)
            self.window!.rootViewController = nav1
            self.window?.makeKeyAndVisible()
        }
        
        setActions()
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        switch application.applicationState {
        case.active:
            print(userInfo)
            
            let aps = userInfo["aps"] as! [String : Any]
            let alert = aps["alert"] as! [String : String]
            let title = alert["title"]
            let body = alert["body"]
            let song1 = aps["song1"] as! String
            let song2 = aps["song2"] as! String
            let song3 = aps["song3"] as! String
            let sound = aps["sound"] as! String
            let endIndex = sound.index(sound.endIndex, offsetBy: -4)
            let sound1 = sound.substring(to: endIndex)
            let rightAnswer = aps["right-answer"] as! String
            
            nav1.popToRootViewController(animated: false)
            let alertController = showAlert(song1: song1, song2: song2, song3: song3, title: title!, body: body!,rightAnswer : rightAnswer)
            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
            
            let url = Bundle.main.url(forResource: sound1, withExtension: "mp3")!
            do {
                player = try AVAudioPlayer(contentsOf: url)
                guard let player = player else { return }
                
                player.prepareToPlay()
                player.play()
            } catch let error as NSError {
                print(error.description)
            }
            break
        case.background:
            
            break
        case.inactive:
            let aps = userInfo["aps"] as! [String : Any]
            let alert = aps["alert"] as! [String : String]
            let title = alert["title"]
            let body = alert["body"]
            let song1 = aps["song1"] as! String
            let song2 = aps["song2"] as! String
            let song3 = aps["song3"] as! String
            let rightAnswer = aps["right-answer"] as! String
            
            nav1.popToRootViewController(animated: false)
            let alertController = showAlert(song1: song1, song2: song2, song3: song3, title: title!, body: body!,rightAnswer : rightAnswer)
            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showAlert(song1 : String,song2 : String,song3 : String,title : String,body : String,rightAnswer : String) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let action1 = UIAlertAction(title: song1, style: .default, handler:{(alert: UIAlertAction!) in self.handler(song: "song1",rightAnswer: rightAnswer)})
        let action2 = UIAlertAction(title: song2, style: .default, handler:{(alert: UIAlertAction!) in self.handler(song: "song2",rightAnswer: rightAnswer)})
        let action3 = UIAlertAction(title: song3, style: .default, handler:{(alert: UIAlertAction!) in self.handler(song: "song3",rightAnswer: rightAnswer)})
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(action3)
        
        return alertController
    }
    
    func handler(song : String, rightAnswer : String) {
        nav1.popToRootViewController(animated: false)
        if song == rightAnswer {
            nav1.pushViewController(CorrectAnswerViewController(), animated: true)
        } else {
            nav1.pushViewController(WrongAnswerViewController(), animated: true)
        }
        player = nil
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
        let aps = userInfo["aps"] as! [String : Any]
        let rightAnswer = aps["right-answer"] as! String
        nav1.popViewController(animated: false)
        if identifier == rightAnswer {
            nav1.pushViewController(CorrectAnswerViewController(), animated: true)
        } else {
            nav1.pushViewController(WrongAnswerViewController(), animated: true)
        }
        completionHandler()
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, completionHandler: @escaping () -> Void) {
        let chosenGenres = UserDefaults.standard.string(forKey: "chosenGenre")!
        if identifier == "yes" {
            switch chosenGenres {
            case "Rock":
                songNotification()
                
            case "Pop":
                songNotificationDespasito()

            case "Classical":
                
                songNotificationBethoven()
            case "Blues":
                songNotificationChuckBerry()

            default:
                break
            }
            
        } else if  identifier == "song1"{
            UserDefaults.standard.set(1, forKey: "showSong")
            //nav.popToRootViewController(animated: false)
            nav.pushViewController(CorrectAnswerViewController(), animated: true)
        } else if identifier == "song2" || identifier == "song3" {
            UserDefaults.standard.set(1, forKey: "showSong")
            nav.pushViewController(WrongAnswerViewController(), animated: true)
            
        } else if identifier == "song2_1" {
            UserDefaults.standard.set(2, forKey: "showSong")
            nav.pushViewController(CorrectAnswerViewController(), animated: true)
        } else if identifier == "song2_2" || identifier == "song2_3" {
            UserDefaults.standard.set(2, forKey: "showSong")
            nav.pushViewController(WrongAnswerViewController(), animated: true)
            
        } else if identifier == "song5_2" {
            UserDefaults.standard.set(4, forKey: "showSong")
            nav.pushViewController(CorrectAnswerViewController(), animated: true)
        } else if identifier == "song5_1" || identifier == "song5_3" {
            UserDefaults.standard.set(4, forKey: "showSong")
            nav.pushViewController(WrongAnswerViewController(), animated: true)
            
        } else if identifier == "song4_3" {
            UserDefaults.standard.set(3, forKey: "showSong")
            nav.pushViewController(CorrectAnswerViewController(), animated: true)
        } else if identifier == "song4_2" || identifier == "song4_1" {
            UserDefaults.standard.set(3, forKey: "showSong")
            nav.pushViewController(WrongAnswerViewController(), animated: true)
        }

        
        completionHandler()
    
        
    }
    
    func setActions() {
        
        if #available(iOS 10.0, *) {
            
            let yes = UNNotificationAction(
                identifier: "yes",
                title: "Yes",
                options: .destructive
            )
            let no = UNNotificationAction(
                identifier: "no",
                title: "No",
                options: .destructive
            )
            
            let category = UNNotificationCategory(
                identifier: "test",
                actions: [yes,no],
                intentIdentifiers: []
            )
            
            let song1_1 = UNNotificationAction(
                identifier: "song1",
                title: "TNT",
                options: .foreground
            )
            let song1_2 = UNNotificationAction(
                identifier: "song2",
                title: "Highway to Hell",
                options: .foreground
            )
            let song1_3 = UNNotificationAction(
                identifier: "song3",
                title: "Shook me all night long ",
                options: .foreground
            )
            
            let category1 = UNNotificationCategory(
                identifier: "song1Notification",
                actions: [song1_1,song1_2,song1_3],
                intentIdentifiers: []
            )
            
            let song2_1 = UNNotificationAction(
                identifier: "song2_1",
                title: "Despasito",
                options: .foreground
            )
            let song2_2 = UNNotificationAction(
                identifier: "song2_2",
                title: "Attention",
                options: .foreground
            )
            let song2_3 = UNNotificationAction(
                identifier: "song2_3",
                title: "Bailando",
                options: .foreground
            )
            
            let category2 = UNNotificationCategory(
                identifier: "song2Notification",
                actions: [song2_1,song2_2,song2_3],
                intentIdentifiers: []
            )
            
            let song3_1 = UNNotificationAction(
                identifier: "song3_1",
                title: "Always on my mind",
                options: .foreground
            )
            let song3_2 = UNNotificationAction(
                identifier: "song3_2",
                title: "Suspicious Minds",
                options: .foreground
            )
            let song3_3 = UNNotificationAction(
                identifier: "share",
                title: "Jailhouse Rock",
                options: .foreground
            )
            
            let category3 = UNNotificationCategory(
                identifier: "song3Notification",
                actions: [song3_1,song3_2,song3_3],
                intentIdentifiers: []
            )
            
            let song4_1 = UNNotificationAction(
                identifier: "song4_1",
                title: "Turkish March",
                options: .foreground
            )
            let song4_2 = UNNotificationAction(
                identifier: "song4_2",
                title: "Moonlight Sonata",
                options: .foreground
            )
            let song4_3 = UNNotificationAction(
                identifier: "song4_3",
                title: "Fur Elise",
                options: .foreground
            )
            
            let category4 = UNNotificationCategory(
                identifier: "song4Notification",
                actions: [song4_1,song4_2,song4_3],
                intentIdentifiers: []
            )
            
            let song5_1 = UNNotificationAction(
                identifier: "song5_1",
                title: "Crossroads",
                options: .foreground
            )
            let song5_2 = UNNotificationAction(
                identifier: "song5_2",
                title: "Johhny B. Goode",
                options: .foreground
            )
            let song5_3 = UNNotificationAction(
                identifier: "song5_3",
                title: "About a Girl",
                options: .foreground
            )
            
            let category5 = UNNotificationCategory(
                identifier: "song5Notification",
                actions: [song5_1,song5_2,song5_3],
                intentIdentifiers: []
            )
            
            
            
            
            UNUserNotificationCenter.current().setNotificationCategories([category,category1,category2,category3,category4,category5])
        }
    }
    
    func songNotification () {
        
        let content = UNMutableNotificationContent()
        
        content.title = NSString.localizedUserNotificationString(forKey: "What's the name of the song?", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "Rock challenge", arguments: nil)
        content.sound = UNNotificationSound(named: "tnt.mp3")
        content.categoryIdentifier = "song1Notification"
        if let path = Bundle.main.path(forResource: "acdc_new1", ofType: "png") {
            let url = URL(fileURLWithPath: path)
            
            do {
                let attachment = try UNNotificationAttachment(identifier: "logo", url: url, options: nil)
                content.attachments = [attachment]
            } catch {
                print("The attachment was not loaded.")
            }
        }
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest.init(identifier: "test", content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.add(request)
        
    }
    
    func songNotificationDespasito () {
        
        let content = UNMutableNotificationContent()
        
        content.title = NSString.localizedUserNotificationString(forKey: "What's the name of the song?", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "Pop chalenge", arguments: nil)
        content.sound = UNNotificationSound(named: "despasitoShort.mp3")
        content.categoryIdentifier = "song2Notification"
        if let path = Bundle.main.path(forResource: "despasitoNotification", ofType: "jpg") {
            let url = URL(fileURLWithPath: path)
            
            do {
                let attachment = try UNNotificationAttachment(identifier: "logo", url: url, options: nil)
                content.attachments = [attachment]
            } catch {
                print("The attachment was not loaded.")
            }
        }
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest.init(identifier: "song2Notification", content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.add(request)
        
    }
    
    func songNotificationSuspicious () {
        
        let content = UNMutableNotificationContent()
        
        content.title = NSString.localizedUserNotificationString(forKey: "What's the name of the song?", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "Music chalenge", arguments: nil)
        content.sound = UNNotificationSound(named: "suspicious.mp3")
        content.categoryIdentifier = "song3Notification"
        if let path = Bundle.main.path(forResource: "elvisNotification", ofType: "jpg") {
            let url = URL(fileURLWithPath: path)
            
            do {
                let attachment = try UNNotificationAttachment(identifier: "logo", url: url, options: nil)
                content.attachments = [attachment]
            } catch {
                print("The attachment was not loaded.")
            }
        }
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest.init(identifier: "song3Notification", content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.add(request)
        
    }
    
    func songNotificationBethoven () {
        
        let content = UNMutableNotificationContent()
        
        content.title = NSString.localizedUserNotificationString(forKey: "What's the name of the song?", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "Classical challenge", arguments: nil)
        content.sound = UNNotificationSound(named: "bethovenShort.mp3")
        content.categoryIdentifier = "song4Notification"
        if let path = Bundle.main.path(forResource: "beethovenNot", ofType: "jpg") {
            let url = URL(fileURLWithPath: path)
            
            do {
                let attachment = try UNNotificationAttachment(identifier: "logo", url: url, options: nil)
                content.attachments = [attachment]
            } catch {
                print("The attachment was not loaded.")
            }
        }
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest.init(identifier: "song4Notification", content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.add(request)
        
    }
    
    func songNotificationChuckBerry () {
        
        let content = UNMutableNotificationContent()
        
        content.title = NSString.localizedUserNotificationString(forKey: "What's the name of the song?", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "Blues challenge", arguments: nil)
        content.sound = UNNotificationSound(named: "chuckShort.mp3")
        content.categoryIdentifier = "song5Notification"
        if let path = Bundle.main.path(forResource: "chuckNotification", ofType: "jpg") {
            let url = URL(fileURLWithPath: path)
            
            do {
                let attachment = try UNNotificationAttachment(identifier: "logo", url: url, options: nil)
                content.attachments = [attachment]
            } catch {
                print("The attachment was not loaded.")
            }
        }
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest.init(identifier: "song5Notification", content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.add(request)
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        SDKApplicationDelegate.shared.application(app, open: url, options: options)
        Twitter.sharedInstance().application(app, open: url, options: options)
        return true
    }
    
    
    //for displaying notification when app is in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert,.sound,.badge])
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString.lowercased())
    }
}
