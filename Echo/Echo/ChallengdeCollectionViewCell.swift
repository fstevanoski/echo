//
//  ChallengdeCollectionViewCell.swift
//  Echo
//
//  Created by Pero on 6/19/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class ChallengdeCollectionViewCell: UICollectionViewCell {
    
    var friendImageView : UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstrants()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        friendImageView = UIImageView()
        friendImageView.image = #imageLiteral(resourceName: "user2")
        friendImageView.backgroundColor = MyVariables.pinkColor
        
        friendImageView.layer.cornerRadius = 25
        friendImageView.layer.masksToBounds = true
        
        contentView.addSubview(friendImageView)
        
    }
    
    func setupConstrants() {
        friendImageView.snp.makeConstraints { make in
            make.left.equalTo(contentView)
            make.top.equalTo(contentView).offset(2)
            make.bottom.equalTo(contentView).offset(-2)
            make.width.equalTo(50)
        }
        
    }
}
