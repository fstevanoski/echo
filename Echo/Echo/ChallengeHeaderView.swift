//
//  ChallengeHeaderView.swift
//  Echo
//
//  Created by Pero on 6/19/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class ChallengeHeaderView: UIView {

    var genreImageView : UIImageView!
    var followingLabel : UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        genreImageView = UIImageView()
        genreImageView.contentMode = .scaleAspectFit
        
        self.addSubview(genreImageView)

    }
    
    func setupConstraints() {
        genreImageView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
        
    }

}
