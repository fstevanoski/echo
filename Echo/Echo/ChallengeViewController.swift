

import UIKit
import UserNotifications

class ChallengeViewController: UIViewController {
    
    var leftHeaderButton : UIButton!
    var tableView : UITableView!
    var segmentedControl : UISegmentedControl!
    var headerView : ChallengeHeaderView!
    var friendsArray = [#imageLiteral(resourceName: "user1"),#imageLiteral(resourceName: "user2"),#imageLiteral(resourceName: "user3"),#imageLiteral(resourceName: "user4"),#imageLiteral(resourceName: "user5"),#imageLiteral(resourceName: "user6"),#imageLiteral(resourceName: "user7"),#imageLiteral(resourceName: "user8"),#imageLiteral(resourceName: "user9"),#imageLiteral(resourceName: "user10"),#imageLiteral(resourceName: "user2"),#imageLiteral(resourceName: "user4"),#imageLiteral(resourceName: "user10"),#imageLiteral(resourceName: "user9")]
    var musicGenres = ["Rock","Pop","Techno","Jazz","Blues","Classical","House","Metal"]
    var genreImages = [#imageLiteral(resourceName: "Rock_Rectangle"),#imageLiteral(resourceName: "pop_Rectangle"),#imageLiteral(resourceName: "techno_Rectangle"),#imageLiteral(resourceName: "jazz_Rectangle"),#imageLiteral(resourceName: "blues_Rectangle"),#imageLiteral(resourceName: "Classical_Rectangle"),#imageLiteral(resourceName: "House_Rectangle"),#imageLiteral(resourceName: "metal_Rectangle")]
    var users = [User]()
    //var user : User!
    var resultArray : [String]!
    var randomOp = 0
    var selectedControl : String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        segmentedControlChangedValue()
        setupConstraints()
        loadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupViews() {
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        
        if let selectedArray = UserDefaults.standard.array(forKey: "selected") {
            let indexSet = selectedArray as! [Int]
            resultArray = indexSet.map { musicGenres[$0] }
            print(resultArray)
        }
        
        self.view.backgroundColor = UIColor.white
        self.title = "Challenge"
        
        leftHeaderButton = UIButton()
        leftHeaderButton.setTitle("Close", for: .normal)
        leftHeaderButton.setTitleColor(MyVariables.pinkColor, for: .normal)
        leftHeaderButton.addTarget(self, action: #selector(onClickCloseButton), for: .touchUpInside)
        leftHeaderButton.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        let leftbarButton = UIBarButtonItem(customView: leftHeaderButton)
        navigationItem.leftBarButtonItem = leftbarButton
        
        let items = resultArray
        segmentedControl = UISegmentedControl(items : items)
        segmentedControl.backgroundColor = UIColor.white
        segmentedControl.tintColor = MyVariables.pinkColor
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(segmentedControlChangedValue), for: .valueChanged)
        
        tableView = UITableView()
        tableView.register(LandingTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.register(FriendsTableViewCell.self, forCellReuseIdentifier: "friendsCell")
        tableView.register(RankingsTableViewCell.self, forCellReuseIdentifier: "randomCell")
        tableView.allowsMultipleSelection = true
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.contentInset = UIEdgeInsetsMake(-50, 0, 0, 0)
        headerView = ChallengeHeaderView(frame : CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 180))

        
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = UIView()
        
        self.view.addSubview(segmentedControl)
        self.view.addSubview(tableView)
        self.view.bringSubview(toFront: segmentedControl)
    }
    
    func setupConstraints() {
        segmentedControl.snp.makeConstraints { make in
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
            make.top.equalTo(self.view).offset(70)
            make.height.equalTo(30)
        }
        tableView.snp.makeConstraints { make in
            make.top.equalTo(self.segmentedControl.snp.bottom)
            make.left.right.equalTo(segmentedControl)
            make.bottom.equalTo(self.view)
        }
    }
    
    func onClickCloseButton() {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func loadData() {
        var names = ["D1B","Mohammed Azhar","Filip Stevanoski","Sechkov Pero","Komal","Ravi","Eldin","Miki","Dejan","Roben","Gaetan le Singe","Pept","Trajche","Aden"]
        var points = [60,517,33,267,204,191,17,172,1333,1602,15,122,120,119]
        var profilePictures = [#imageLiteral(resourceName: "user1"),#imageLiteral(resourceName: "user2"),#imageLiteral(resourceName: "user3"),#imageLiteral(resourceName: "user4"),#imageLiteral(resourceName: "user5"),#imageLiteral(resourceName: "user6"),#imageLiteral(resourceName: "user7"),#imageLiteral(resourceName: "user8"),#imageLiteral(resourceName: "user9"),#imageLiteral(resourceName: "user10"),#imageLiteral(resourceName: "user2"),#imageLiteral(resourceName: "user4"),#imageLiteral(resourceName: "user10"),#imageLiteral(resourceName: "user9")]
        var countryImages = [#imageLiteral(resourceName: "americanFlag"),#imageLiteral(resourceName: "macedonianFlag"),#imageLiteral(resourceName: "mexicanFlag"),#imageLiteral(resourceName: "serbianFlag"),#imageLiteral(resourceName: "bosnianFlag"),#imageLiteral(resourceName: "croatianFlag")]
        var countries = ["United States","Macedonia","Mexico","Serbia","Bosnia and Herzegovina","Croatia"]
        for i in 0...13 {
            users.append(User(username: names[i], points: points[i], profilePicture: profilePictures[i], countryImage: countryImages[i%6],country : countries[i%6]))
        }
        users.sort() {
            $0.0.points > $0.1.points
        }
    }
    func handler () {
        getNotification()
    }
    
    func segmentedControlChangedValue() {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            selectedControl = resultArray.first
            print(selectedControl)
            let index = musicGenres.index(of: selectedControl)
            print(index!)
            let i = genreImages.index(after: index! - 1)
            headerView.genreImageView.image = genreImages[i]
            UserDefaults.standard.set(selectedControl, forKey: "chosenGenre")
        case 1:
            selectedControl = resultArray[1]
            print(selectedControl)
            let index = musicGenres.index(of: selectedControl)
            print(index!)
            let i = genreImages.index(after: index! - 1)
            headerView.genreImageView.image = genreImages[i]
            UserDefaults.standard.set(selectedControl, forKey: "chosenGenre")
        case 2:
            selectedControl = resultArray[2]
            print(selectedControl)
            let index = musicGenres.index(of: selectedControl)
            print(index!)
            let i = genreImages.index(after: index! - 1)
            headerView.genreImageView.image = genreImages[i]
            UserDefaults.standard.set(selectedControl, forKey: "chosenGenre")
        case 3:
            selectedControl = resultArray[3]
            print(selectedControl)
            let index = musicGenres.index(of: selectedControl)
            print(index!)
            let i = genreImages.index(after: index! - 1)
            headerView.genreImageView.image = genreImages[i]
            UserDefaults.standard.set(selectedControl, forKey: "chosenGenre")
        default: break
            
        }
        
    }
    
    func getNotification() {
        let content = UNMutableNotificationContent()
        
        content.title = NSString.localizedUserNotificationString(forKey: "Pero Seckov sent you a challenge", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "Would you like to play?", arguments: nil)
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = "test"
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 3, repeats: false)
        let request = UNNotificationRequest.init(identifier: "test", content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.add(request)
    }
}




extension ChallengeViewController : UNUserNotificationCenterDelegate {
    
    //for displaying notification when app is in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert,.sound,.badge])
    }
}

extension ChallengeViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let chosenGenre = UserDefaults.standard.string(forKey: "chosenGenre")!
        let alert = UIAlertController(title: chosenGenre, message: "Challenge Filip Stevanoski in \(chosenGenre)?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler:{(alert: UIAlertAction!) in self.handler() }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
extension ChallengeViewController : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return friendsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! ChallengdeCollectionViewCell
        cell.friendImageView.image = friendsArray[indexPath.row]
        return cell
    }
    
    
}

extension ChallengeViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                return 52
            } else {
                return 32
            }
        }
        return 52
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                randomOp = users.count
                tableView.reloadData()
                let randomSelection = Int(arc4random_uniform(UInt32(users.count)))
                print(randomSelection)
                let indexPath = IndexPath(row: randomSelection, section: 1)
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                randomOp = 0
                tableView.reloadData()
            }
        }
    }
    
}

extension ChallengeViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        return randomOp + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LandingTableViewCell
                cell.arrowImage.isHidden = true
                cell.label.text = "Challenge a friend"
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "friendsCell", for: indexPath) as! FriendsTableViewCell
                cell.collectionView.dataSource = self
                cell.collectionView.delegate = self
                cell.collectionView.register(ChallengdeCollectionViewCell.self, forCellWithReuseIdentifier: "collectionViewCell")
                return cell
            }
        } else {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LandingTableViewCell
                cell.arrowImage.isHidden = true
                cell.label.text = "Random opponent"
                if randomOp == users.count {
                    cell.isSelected = true
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "randomCell", for: indexPath) as! RankingsTableViewCell
                cell.setupContent(rank: "\(indexPath.row)", name: users[indexPath.row - 1].username, points: users[indexPath.row - 1].points, index: indexPath.row - 1,flag : users[indexPath.row - 1].countryImage,profilePicture : users[indexPath.row - 1].profilePicture)
                return cell
            }
        }
        
        
    }
    
}
