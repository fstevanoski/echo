//
//  CorrectAnswerTableViewCell.swift
//  Echo
//
//  Created by Pero on 6/23/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import Foundation
import UIKit


class CorrectAnswerTableViewCell : UITableViewCell {
    
    var cellImageView : UIImageView!
    var label : UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.accessoryType = .disclosureIndicator
        self.selectionStyle = .none
        
        cellImageView = UIImageView()
        cellImageView.contentMode = .scaleAspectFit
        
        label = UILabel()
        label.textColor = UIColor.black
        label.font = MyVariables.fontRegular20
        
        contentView.addSubview(cellImageView)
        contentView.addSubview(label)
    }
    
    func setupConstraints() {
        cellImageView.snp.makeConstraints { make in
            make.centerY.equalTo(contentView.snp.centerY)
            make.left.equalTo(contentView).offset(15)
            make.width.height.equalTo(35)
        }
        label.snp.makeConstraints { make in
            make.left.equalTo(cellImageView.snp.right).offset(20)
            make.top.bottom.right.equalTo(contentView)
        }
    }
    
    func setupCell(image : UIImage,text : String) {
        cellImageView.image = image
        label.text = text
    }
}
