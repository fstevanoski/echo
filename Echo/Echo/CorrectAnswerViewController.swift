//
//  CorrectAnswerViewController.swift
//  Echo
//
//  Created by Pero on 6/12/17.
//  Copyright © 2017 shortway. All rights reserved.
//
import UIKit

class CorrectAnswerViewController: UIViewController {
    
    var artistImageView : UIImageView!
    var okButton : UIButton!
    var tableView : UITableView!
    var items = ["Play this song","AC/DC Accessories","Buy tickets for next concerts"]
    var images = [#imageLiteral(resourceName: "music_note"),#imageLiteral(resourceName: "shoppingCart"),#imageLiteral(resourceName: "tickets")]
    var songs = [Int]()
    var count1 = false
    var count2 = false
    var count3 = false
    var count4 = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addPoints()
        setupView()
        setupConstraints()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        navigationController?.navigationBar.tintColor = MyVariables.pinkColor
//        navigationController?.navigationBar.isTranslucent = true
//    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.isTranslucent = true
        
    }
    
    func setupView() {
        if UserDefaults.standard.value(forKey: "correctSongs") != nil {
        songs = UserDefaults.standard.array(forKey: "correctSongs") as! [Int]
        } else {
            songs = [Int]()
        }
        
        self.navigationItem.hidesBackButton = true
        self.view.backgroundColor = UIColor.white
        self.navigationItem.prompt = "Congratulations"
        self.title = "Correct answer +10 points"
        
       
        artistImageView = UIImageView()
        
        let songNumb = UserDefaults.standard.integer(forKey: "showSong")
        switch songNumb {
           
        case 1:
            count1 = UserDefaults.standard.bool(forKey: "count1")
            if count1 == false {
            UserDefaults.standard.set(true, forKey: "count1")
            songs.append(0)
            }
            
            UserDefaults.standard.set("TNT", forKey: "song")
            UserDefaults.standard.set("AC/DC", forKey: "artist")
            UserDefaults.standard.set(UIImagePNGRepresentation(#imageLiteral(resourceName: "acdc_new1")), forKey: "artistImage")
            UserDefaults.standard.set("tntLong", forKey: "playSong")
            UserDefaults.standard.set(0, forKey: "counter")
            artistImageView.image = #imageLiteral(resourceName: "acdc_new1")
            
        case 2:
            count2 = UserDefaults.standard.bool(forKey: "count2")
            if count2 == false {
                UserDefaults.standard.set(true, forKey: "count2")
            songs.append(1)
            }
            UserDefaults.standard.set("Despasito", forKey: "song")
            UserDefaults.standard.set("Luis Fonsi", forKey: "artist")
            UserDefaults.standard.set(UIImagePNGRepresentation(#imageLiteral(resourceName: "despasitoNotification.jpg")), forKey: "artistImage")
            UserDefaults.standard.set("despasitoLong", forKey: "playSong")
            UserDefaults.standard.set(1, forKey: "counter")
            artistImageView.image = #imageLiteral(resourceName: "despasitoNotification.jpg")
        case 3:
            count3 = UserDefaults.standard.bool(forKey: "count3")
            if count3 == false {
                UserDefaults.standard.set(true, forKey: "count3")
            songs.append(2)
            }
            UserDefaults.standard.set("Fur Elise", forKey: "song")
            UserDefaults.standard.set("Ludvig van Bethoveen", forKey: "artist")
            UserDefaults.standard.set(UIImagePNGRepresentation(#imageLiteral(resourceName: "beethovenNot.jpg")), forKey: "artistImage")
            UserDefaults.standard.set("bethovenLong", forKey: "playSong")
            UserDefaults.standard.set(3, forKey: "counter")
            artistImageView.image = #imageLiteral(resourceName: "beethovenNot.jpg")
        case 4:
           count4 = UserDefaults.standard.bool(forKey: "count4")
            if count4 == false {
                UserDefaults.standard.set(true, forKey: "count4")
            songs.append(3)
            }
           UserDefaults.standard.set("Johnny B. Goode", forKey: "song")
           UserDefaults.standard.set("Chuck Berry", forKey: "artist")
           UserDefaults.standard.set(UIImagePNGRepresentation(#imageLiteral(resourceName: "chuckNotification.jpg")), forKey: "artistImage")
           UserDefaults.standard.set("chuckLong", forKey: "playSong")
           UserDefaults.standard.set(2, forKey: "counter")
            artistImageView.image = #imageLiteral(resourceName: "chuckNotification.jpg")
        default:
            break
        }
        
        artistImageView.contentMode = .scaleAspectFit
        
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 70, bottom: 0, right: 0)
        tableView.register(CorrectAnswerTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView(frame : CGRect.zero)
        
        okButton = UIButton()
        okButton.setTitle("OK", for: .normal)
        okButton.setTitleColor(UIColor.white, for: .normal)
        okButton.titleLabel?.font = MyVariables.fontRegular20
        okButton.backgroundColor = MyVariables.pinkColor
        okButton.layer.cornerRadius = 15
        okButton.addTarget(self, action: #selector(okButtonTouched), for: .touchUpInside)
        
        self.view.addSubview(artistImageView)
        self.view.addSubview(tableView)
        self.view.addSubview(okButton)
        print("see2 \(songs.count)")
    }
    
    func setupConstraints() {
        
        artistImageView.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(110)
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
            make.height.equalTo(self.view.frame.size.width-60)
        }
        
        tableView.snp.makeConstraints { make in
            make.top.equalTo(artistImageView.snp.bottom).offset(10)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(okButton.snp.top)
        }
        
        okButton.snp.makeConstraints { make in
            make.bottom.equalTo(self.view).offset(-10)
            make.left.equalTo(self.view).offset(10)
            make.right.equalTo(self.view).offset(-10)
            make.height.equalTo(self.view.snp.height).dividedBy(10)
        }
    }
    

    
    func addPoints() {
        var points = UserDefaults.standard.value(forKey: "points") as! Int
        points += 10
        UserDefaults.standard.set(points, forKey: "points")
    }
    
    func okButtonTouched() {
        UserDefaults.standard.set(songs, forKey: "correctSongs")
        _ = self.navigationController?.popViewController(animated: true)
        print("see3 \(songs.count)")
    }
}

extension CorrectAnswerViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            navigationController?.pushViewController(PlaySongViewController(), animated: true)
        }
    }
}

extension CorrectAnswerViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CorrectAnswerTableViewCell
        cell.setupCell(image: images[indexPath.row], text: items[indexPath.row])
        return cell
    }
}

extension Notification.Name {
    static let points = Notification.Name("points")
}
