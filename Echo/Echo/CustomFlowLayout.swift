//
//  CustomFlowLayout.swift
//  Echo
//
//  Created by Pero on 5/25/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class CustomFlowLayout : UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        setUpLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpLayout() {
        minimumInteritemSpacing = 0
        minimumLineSpacing = 10
        scrollDirection = .vertical
        //headerReferenceSize = CGSize(width: 0, height: 20)
    }
    override var itemSize: CGSize {
        set{}
        get{
            let x = (self.collectionView?.frame.width)!-10
            let size = CGSize(width: x/2, height: x/2 - 10)
            return size
        }
        
    }
    
}
