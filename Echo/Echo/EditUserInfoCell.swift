//
//  UserInfoCell.swift
//  Echo
//
//  Created by Pero on 5/25/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit


protocol  EditUserCellDelegate {
    func changeConstraints(value : Int)
    func backToNormal()
}
class EditUserInfoCell: UITableViewCell , UIPickerViewDelegate {

    var infoLabel : UILabel!
    var editInfoTextField : UITextField!
    let datepicker = UIDatePicker()
    let sexPickerView = UIPickerView()
    let countryPickerView = UIPickerView()
    let sexpicker = ["Not Set","Male","Female"]
    let notificationTimePickerView = UIPickerView()
    
    var toolBar : UIToolbar!
    var label  : UILabel!
    var delegate : EditUserCellDelegate?
    let countriesArray = ["United States", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Indian Ocean Territory", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma (Myanmar)", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Cook Islands", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Democratic Republic of the Congo", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "North Korea", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Republic of the Congo", "Romania", "Russia", "Rwanda", "Saint Barthelemy", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Martin", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "US Virgin Islands", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"]
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        infoLabel = UILabel()
        infoLabel.textColor = UIColor.black
        infoLabel.font = MyVariables.fontBold20
        infoLabel.textAlignment = .left
        
        datepicker.backgroundColor = UIColor.white
        sexPickerView.delegate = self
        sexPickerView.tintColor = UIColor.black
        sexPickerView.backgroundColor = UIColor.white
        sexPickerView.showsSelectionIndicator = true
        sexPickerView.tag = 1
        
        countryPickerView.delegate = self
        countryPickerView.tintColor = UIColor.black
        countryPickerView.backgroundColor = UIColor.white
        countryPickerView.tag = 2
        
        
        toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        toolBar.barTintColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "DONE", style: .done, target: self, action: #selector(onClickDoneButton))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton,doneButton], animated: true)
        
        editInfoTextField = UITextField()
        editInfoTextField.textAlignment = .right
        editInfoTextField.textColor = UIColor.black
        editInfoTextField.isUserInteractionEnabled = false
        editInfoTextField.autocorrectionType = .no
        editInfoTextField.font = MyVariables.fontRegular15
        editInfoTextField.delegate = self
        
        label  = UILabel()
        label.textAlignment = .right
        label.text = "Not Set"
        label.font = MyVariables.fontRegular15
        label.textColor = UIColor.lightGray
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.isHidden = true
        
        contentView.addSubview(infoLabel)
        contentView.addSubview(editInfoTextField)
        contentView.addSubview(label )
    }

    func setupConstraints() {
        infoLabel.snp.makeConstraints { make in
            make.top.equalTo(contentView)
            make.left.equalTo(contentView).offset(10)
            make.width.equalTo(contentView.snp.width).dividedBy(2)
            make.height.equalTo(contentView.snp.height)
        }
        editInfoTextField.snp.makeConstraints { make in
            make.top.equalTo(contentView)
            make.left.equalTo(infoLabel.snp.right)
            make.right.equalTo(contentView).offset(-10)
            make.height.equalTo(infoLabel)
        }
        label.snp.makeConstraints { make in
            make.top.equalTo(contentView)
            make.left.equalTo(infoLabel.snp.right)
            make.right.equalTo(contentView).offset(-10)
            make.height.equalTo(infoLabel)
        }
    }
    
    func onClickDoneButton() {
        delegate?.backToNormal()
        editInfoTextField.resignFirstResponder()
    }
    
    func editInfo (key : String) {
        if let text = UserDefaults.standard.string(forKey: key) {
            if text == ""{
                editInfoTextField.placeholder = "Not Set >"
            } else {
               editInfoTextField.text = text
            }
        } else {
            editInfoTextField.placeholder = "Not Set >"
        }
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == sexPickerView {
            return sexpicker.count
        } else  {
            return countriesArray.count
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == sexPickerView {
            return sexpicker[row]
        } else  {
            return countriesArray[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == sexPickerView {
            editInfoTextField.text =  sexpicker[row]
        } else  {
            editInfoTextField.text = countriesArray[row]
        }
    }
}

extension EditUserInfoCell : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        editInfoTextField.textColor = MyVariables.pinkColor
        if infoLabel.text == "Date of Birth" {
            delegate?.changeConstraints(value: -40)
            datepicker.datePickerMode = UIDatePickerMode.date
            textField.inputView = datepicker
            editInfoTextField.inputAccessoryView = toolBar
            datepicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        } else if infoLabel.text == "Sex" {
            delegate?.changeConstraints(value : -70)
            editInfoTextField.inputView = sexPickerView
            editInfoTextField.inputAccessoryView = toolBar
        } else if infoLabel.text == "Country"{
            editInfoTextField.inputView = countryPickerView
            editInfoTextField.inputAccessoryView = toolBar
            delegate?.changeConstraints(value: 120)
        }
    }

    func datePickerValueChanged() {
        let dateFormat = DateFormatter()
        dateFormat.dateStyle = DateFormatter.Style.medium
        dateFormat.timeStyle = DateFormatter.Style.none
        editInfoTextField.text = dateFormat.string(from: datepicker.date)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var key = ""
        if infoLabel.text == "First Name" {
            key = "firstName"
        } else if infoLabel.text == "Last Name" {
            key = "lastName"
        } else if infoLabel.text == "Username" {
            key = "username"
        } else if infoLabel.text == "Date of Birth" {
            key = "date"
        } else if infoLabel.text == "Sex" {
            key = "sex"
        } else if infoLabel.text == "Music Genres" {
            key = "music"
        } else {
            key = "country"
        }
        UserDefaults.standard.set(textField.text, forKey: key)
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        editInfoTextField.textColor = UIColor.black
        
        var key = ""
        if infoLabel.text == "First Name" {
            key = "firstName"
        } else if infoLabel.text == "Last Name" {
            key = "lastName"
        } else if infoLabel.text == "Username" {
            key = "username"
        } else if infoLabel.text == "Date of Birth" {
            key = "date"
        } else if infoLabel.text == "Sex" {
            key = "sex"
        } else if infoLabel.text == "Music Genres" {
            key = "music"
        } else  {
            key = "country"
        }
        UserDefaults.standard.set(textField.text, forKey: key)
        textField.resignFirstResponder()
    }
}

