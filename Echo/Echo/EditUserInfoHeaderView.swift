
//
//  HeaderViewSecondActivationScreen.swift
//  Echo
//
//  Created by Pero on 5/23/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class EditUserInfoHeaderView: UIView {
    
    var userImageView : UIImageView!
    var myInfoLabel : UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupView() {
        userImageView = UIImageView()
        userImageView.contentMode = .scaleAspectFill
        userImageView.layer.cornerRadius = 40
        if let imageData = UserDefaults.standard.data(forKey: "image") {
            userImageView.image = UIImage(data: imageData)
        } else {
            userImageView.image = #imageLiteral(resourceName: "user")
        }
        userImageView.layer.masksToBounds = true
        userImageView.isUserInteractionEnabled = false
        
        if let imageData = UserDefaults.standard.data(forKey: "image") {
            userImageView.image = UIImage(data: imageData)
        } else {
            userImageView.image = #imageLiteral(resourceName: "user")
        }
        
        myInfoLabel = UILabel()
        myInfoLabel.textAlignment = .center
        myInfoLabel.textColor = UIColor.black
        myInfoLabel.font = UIFont.systemFont(ofSize: 25)
        myInfoLabel.text = UserDefaults.standard.string(forKey: "username")
        
        self.addSubview(userImageView)
        self.addSubview(myInfoLabel)
    }
    
    func setupConstraints() {
        userImageView.snp.makeConstraints { make in
            make.top.equalTo(self).offset(10)
            make.centerX.equalTo(self.snp.centerX)
            make.width.equalTo(80)
            make.height.equalTo(80)
        }
        myInfoLabel.snp.makeConstraints { make in
            make.top.equalTo(userImageView.snp.bottom)
            make.centerX.equalTo(self.snp.centerX)
            make.width.equalTo(self)
            make.height.equalTo(50)
        }
    }
    
}
