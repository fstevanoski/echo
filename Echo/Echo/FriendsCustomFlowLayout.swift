//
//  FriendsCustomFlowLayout.swift
//  Echo
//
//  Created by Pero on 6/19/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class FriendsCustomFlowLayout: UICollectionViewFlowLayout {

    override init() {
        super.init()
        setUpLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setUpLayout() {
        minimumInteritemSpacing = 0
        minimumLineSpacing = 10
        scrollDirection = .horizontal
    }
    
    override var itemSize: CGSize {
        set{}
        get{
            //let x = ((self.collectionView?.frame.size.width)!/5
          let x = (self.collectionView?.frame.width)!/5 - 20
            let size = CGSize(width: x, height: (self.collectionView?.frame.height)!)
            return size
        }
        
    }
}
