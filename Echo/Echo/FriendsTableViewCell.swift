//
//  FriendsTableViewCell.swift
//  Echo
//
//  Created by Pero on 6/19/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class FriendsTableViewCell: UITableViewCell {

    var collectionView : UICollectionView!
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: FriendsCustomFlowLayout())
        collectionView.backgroundColor = UIColor.white
        collectionView.showsHorizontalScrollIndicator = false
        
        contentView.addSubview(collectionView)
    }
    
    func setupConstraints() {
        collectionView.snp.makeConstraints { make in
            make.top.bottom.equalTo(contentView)
            make.left.equalTo(contentView).offset(5)
            make.right.equalTo(contentView).offset(-5)
        }
    }

}
