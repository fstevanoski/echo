//
//  GenreTableViewCell.swift
//  Echo
//
//  Created by Miki Dimitrov on 6/14/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class GenresTableViewCell : UITableViewCell {
    
    var collectionView : UICollectionView!
    var noGenresLabel : UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        collectionView.reloadData()
    }
    func setupView() {
        collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: CustomFlowLayout())
        collectionView.backgroundColor = UIColor.white
        
        noGenresLabel = UILabel()
        noGenresLabel.text = "There are no genres choosen"
        noGenresLabel.textColor = UIColor.black
        noGenresLabel.font = UIFont.systemFont(ofSize: 15)
        noGenresLabel.textAlignment = .center
        noGenresLabel.isHidden = true
        
        contentView.addSubview(collectionView)
        contentView.addSubview(noGenresLabel)
    }
    
    func setupConstraints() {
        collectionView.snp.makeConstraints { make in
            make.edges.equalTo(contentView)
        }
        noGenresLabel.snp.makeConstraints { make in
            make.edges.equalTo(contentView)
        }
    }
}
