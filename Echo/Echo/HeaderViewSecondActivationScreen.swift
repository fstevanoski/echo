//
//  HeaderViewSecondActivationScreen.swift
//  Echo
//
//  Created by Pero on 5/23/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class HeaderViewSecondActivationScreen: UIView {
    
    var userImageView : UIImageView!
    var myInfoLabel : UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupView() {
        userImageView = UIImageView()
        userImageView.contentMode = .scaleToFill
        userImageView.layer.cornerRadius = 60
        userImageView.layer.masksToBounds = true
        userImageView.isUserInteractionEnabled = true
        
        
        if let imageData = UserDefaults.standard.data(forKey: "image") {
            userImageView.image = UIImage(data: imageData)
        } else {
            userImageView.image = #imageLiteral(resourceName: "user")
        }
        
        myInfoLabel = UILabel()
        myInfoLabel.font = MyVariables.fontBold20
        myInfoLabel.text = "My info"
        myInfoLabel.textAlignment = .center
        myInfoLabel.textColor = UIColor.black
        
        self.addSubview(userImageView)
        self.addSubview(myInfoLabel)
    }
    
    func setupConstraints() {
        userImageView.snp.makeConstraints { make in
            make.top.equalTo(self).offset(10)
            make.left.equalTo(self).offset(self.frame.size.width/2-60)
            print(self.frame.size.width)
            //make.centerX.equalTo(self.snp.centerX)
            make.width.equalTo(120)
            make.height.equalTo(120)
        }
        myInfoLabel.snp.makeConstraints { make in
            make.top.equalTo(userImageView.snp.bottom)
            make.left.equalTo(userImageView)
            make.width.equalTo(120)
            make.height.equalTo(50)
        }
    }
    
}
