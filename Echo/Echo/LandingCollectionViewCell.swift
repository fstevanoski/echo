//
//  LandingCollectionViewCell.swift
//  Echo
//
//  Created by Pero on 5/25/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class LandingCollectionViewCell: UICollectionViewCell {
    
    var imageView : UIImageView!
    var titleGenre : UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
        
        titleGenre = UILabel()
        titleGenre.backgroundColor = UIColor.clear
        titleGenre.font = MyVariables.fontBold20
        titleGenre.textColor = UIColor.white
        titleGenre.textAlignment = .left
        
        
        contentView.addSubview(imageView)
        contentView.addSubview(titleGenre)
    }
    
    
    

    func setupConstraints() {
        imageView.snp.makeConstraints { make in
            make.top.left.right.bottom.equalTo(contentView)
        
        }
        titleGenre.snp.makeConstraints { make in
            make.top.equalTo(self).offset(10)
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-10)
            make.height.equalTo(20)
        }
        
    }
}
