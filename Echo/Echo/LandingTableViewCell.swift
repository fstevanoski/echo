//
//  LandingTableViewCell.swift
//  Echo
//
//  Created by Pero on 5/26/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class LandingTableViewCell : UITableViewCell {
    
    var label : UILabel!
    var arrowImage : UIImageView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        selectionStyle = .none
        label = UILabel()
        label.textColor = UIColor.gray
        label.textAlignment = .center
        label.font = MyVariables.fontRegular20
        
        arrowImage = UIImageView()
        arrowImage.contentMode = .scaleAspectFit
            
        contentView.addSubview(label)
        contentView.addSubview(arrowImage)
    }
    
    func setupConstraints() {
        label.snp.makeConstraints { make in
            make.top.bottom.equalTo(contentView)
            make.centerX.equalTo(contentView.snp.centerX)
            make.width.equalTo(contentView.snp.width).dividedBy(2)
        }
        arrowImage.snp.makeConstraints { make in
         make.right.equalTo(contentView).offset(-10)
            make.top.equalTo(contentView)
            make.height.equalTo(contentView)
            make.width.equalTo(20)
            
        }
    }
    
}
