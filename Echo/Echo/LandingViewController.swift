//
//  LandingViewController.swift
//  Echo
//
//  Created by Pero on 5/25/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class LandingViewController: UIViewController {
    var titleLabel : UILabel!
    var headerButton : UIButton!
    var collectionView : UICollectionView!
    var genreImages = [#imageLiteral(resourceName: "rock_color"),#imageLiteral(resourceName: "pop_color"),#imageLiteral(resourceName: "techno_color"),#imageLiteral(resourceName: "jazz"),#imageLiteral(resourceName: "blues_color"),#imageLiteral(resourceName: "classical_color"),#imageLiteral(resourceName: "house_color"),#imageLiteral(resourceName: "metal_color")]
    var musicGenres = ["Rock","Pop","Techno","Jazz","Blues","Classical","House","Metal"]
    var tableView : UITableView!
    var selectedArray = [Int]()
    var genres = 0
    var rankings = 0
    var users = [User]()
    var user : User!
    var challengeButton : UIButton!
    var playlistButton : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initializeUser()
        loadData()
        navigationController?.navigationBar.tintColor = MyVariables.pinkColor
        navigationController?.navigationBar.isTranslucent = true
        if UserDefaults.standard.array(forKey: "selected") as? [Int] != nil {
            selectedArray =  UserDefaults.standard.array(forKey: "selected") as! [Int]
            print(selectedArray)
        }
        
        tableView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.isTranslucent = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isTranslucent = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func setupView() {
        
        self.view.backgroundColor = UIColor.white
        self.title = "ECHO"
        headerButton = UIButton()
        if let imageData = UserDefaults.standard.value(forKey: "image") as? Data {
            headerButton.setImage(UIImage(data : imageData), for: .normal)
        }
        else {
            headerButton.setImage(#imageLiteral(resourceName: "user"), for: .normal)
        }
        headerButton.layer.masksToBounds = true
        headerButton.layer.cornerRadius = 15
        headerButton.addTarget(self, action: #selector(onClickHeaderButton), for: .touchUpInside)
        headerButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        playlistButton = UIButton()
        playlistButton.frame = CGRect(x: 0, y: 0, width: 40, height: 30)
        playlistButton.setImage(#imageLiteral(resourceName: "playlistIcon"), for: .normal)
        playlistButton.addTarget(self, action: #selector(playlistButtonTouched), for: .touchUpInside)
        
        let barButton = UIBarButtonItem(customView: headerButton)
        let leftBarButton = UIBarButtonItem(customView: playlistButton)
        navigationItem.rightBarButtonItem = barButton
        navigationItem.leftBarButtonItem = leftBarButton
        //self.navigationItem.hidesBackButton = false
        
        tableView = UITableView()
        tableView.register(GenresTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.register(LandingTableViewCell.self, forCellReuseIdentifier: "test")
        tableView.register(MoreGenresCell.self, forCellReuseIdentifier: "genres")
        tableView.register(RankingsTableViewCell.self, forCellReuseIdentifier: "rankingsCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.allowsMultipleSelection = true
        
        challengeButton = UIButton()
        challengeButton.setTitle("Challenge", for: .normal)
        challengeButton.titleLabel?.font = MyVariables.fontRegular20
        challengeButton.setTitleColor(UIColor.white, for: .normal)
        challengeButton.backgroundColor = MyVariables.pinkColor
        challengeButton.addTarget(self, action: #selector(onClickChallenge), for: .touchUpInside)
        challengeButton.layer.cornerRadius = 15
        
        self.view.addSubview(tableView)
        self.view.addSubview(challengeButton)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(15)
            make.left.equalTo(self.view).offset(10)
            make.right.equalTo(self.view).offset(-10)
            make.bottom.equalTo(challengeButton.snp.top).offset(-10)
        }
        challengeButton.snp.makeConstraints { make in
            make.bottom.equalTo(self.view).offset(-10)
            make.left.equalTo(self.view).offset(10)
            make.right.equalTo(self.view).offset(-10)
            make.height.equalTo(self.view.snp.height).dividedBy(10)
            
        }
    }
    
    func onClickChallenge() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let nav = appDelegate.nav
        nav.pushViewController(ChallengeViewController(), animated: true)
        self.present(nav, animated: true, completion: nil)
        //self.navigationController?.pushViewController(ChallengeViewController(), animated: true)
        
    }
    
    func onClickHeaderButton() {
        let navUserInfoViewController : UINavigationController = UINavigationController(rootViewController: UserInfoViewController())
        self.present(navUserInfoViewController, animated: true, completion: nil)
    }
    
    func initializeUser() {
        var points = 0
        var profilePicture : UIImage!
         let username = UserDefaults.standard.value(forKey: "username") as! String
        
        if  UserDefaults.standard.value(forKey: "points") as? Int != nil {
            points = UserDefaults.standard.value(forKey: "points") as! Int
        }
        
        
        if  let profilePictureData = UserDefaults.standard.value(forKey: "image") as? Data {
            profilePicture = UIImage(data: profilePictureData)
        } else {
            profilePicture = #imageLiteral(resourceName: "user")
        }
        
        user = User(username: username, points: points, profilePicture: profilePicture!, countryImage: #imageLiteral(resourceName: "macedonianFlag"), country: "Macedonia")
    }
    
    func loadData() {
        users = [User]()
        var names = ["D1B","Mohammed Azhar","Filip Stevanoski","Sechkov Pero","Komal","Ravi","Eldin","Miki","Dejan","Roben","Gaetan le Singe","Pept","Trajche","Aden"]
        var points = [60,517,33,267,204,191,17,172,1333,1602,15,122,120,119]
        var profilePictures = [#imageLiteral(resourceName: "user1"),#imageLiteral(resourceName: "user2"),#imageLiteral(resourceName: "user3"),#imageLiteral(resourceName: "user4"),#imageLiteral(resourceName: "user5"),#imageLiteral(resourceName: "user6"),#imageLiteral(resourceName: "user7"),#imageLiteral(resourceName: "user8"),#imageLiteral(resourceName: "user9"),#imageLiteral(resourceName: "user10"),#imageLiteral(resourceName: "user2"),#imageLiteral(resourceName: "user4"),#imageLiteral(resourceName: "user10"),#imageLiteral(resourceName: "user9")]
        var countryImages = [#imageLiteral(resourceName: "americanFlag"),#imageLiteral(resourceName: "macedonianFlag"),#imageLiteral(resourceName: "mexicanFlag"),#imageLiteral(resourceName: "serbianFlag"),#imageLiteral(resourceName: "bosnianFlag"),#imageLiteral(resourceName: "croatianFlag")]
        var countries = ["United States","Macedonia","Mexico","Serbia","Bosnia and Herzegovina","Croatia"]
        for i in 0...13 {
            users.append(User(username: names[i], points: points[i], profilePicture: profilePictures[i], countryImage: countryImages[i%6],country : countries[i%6]))
        }
        users.append(user)
        
        users.sort() {
            $0.0.points > $0.1.points
        }
    }
    
    func playlistButtonTouched() {
        navigationController?.pushViewController(SongsViewController(), animated: true)
        
    }
    
}


extension LandingViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! LandingCollectionViewCell
        cell.imageView.image = genreImages[selectedArray[indexPath.row]]
        cell.imageView.layer.cornerRadius = 15
        cell.titleGenre.text = musicGenres[selectedArray[indexPath.row]]
        return cell
    }
}

extension LandingViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return self.view.frame.width
        } else if indexPath.section == 2{
            if indexPath.row == 0 {
                return 65
            }
            return 32
        } else {
            return 65
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                genres = musicGenres.count
                rankings = 0
                tableView.reloadData()
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            } else {
                let cell = tableView.cellForRow(at: indexPath) as! MoreGenresCell
                if selectedArray.count < 4 {
                    selectedArray.append(indexPath.row - 1)
                    print(selectedArray)
                    UserDefaults.standard.set(selectedArray, forKey: "selected")
                    cell.selectedImageView.isHidden = false
                    print(selectedArray)
                } else {
                    cell.isSelected = false
                    tableView.deselectRow(at: indexPath, animated: false)
                    let alert = UIAlertController(title: "", message: "You've reached your limit", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    print(selectedArray)
                }
            }
            
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                genres = 0
                rankings = users.count
                
                tableView.reloadData()
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            } else {
                navigationController?.pushViewController(UserProfileViewController(), animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 2 {
            if indexPath.row == 0 {
                genres = 0
                rankings = 0
                tableView.reloadSections(NSIndexSet(index : indexPath.section) as IndexSet, with: .automatic)
                tableView.reloadData()
                //tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                genres = 0
                rankings = 0
                tableView.reloadSections(NSIndexSet(index : indexPath.section) as IndexSet, with: .automatic)
                tableView.reloadData()
                //tableView.scrollToRow(at: indexPath, at: .middle, animated: true)
            
            } else {
                let cell = tableView.cellForRow(at: indexPath) as! MoreGenresCell
                cell.selectedImageView.isHidden = true
                selectedArray = selectedArray.filter {$0 != indexPath.row - 1}
                UserDefaults.standard.set(selectedArray, forKey: "selected")
                print(selectedArray)
            }
        }
    }
}

extension LandingViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return genres + 1
        } else {
            return rankings + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GenresTableViewCell
            cell.collectionView.dataSource = self
            cell.collectionView.register(LandingCollectionViewCell.self, forCellWithReuseIdentifier: "collectionCell")
            if UserDefaults.standard.value(forKey: "selected") != nil {
                if let selectedIndexes = UserDefaults.standard.array(forKey: "selected") as? [Int] {
                    if selectedIndexes.count == 0 {
                        cell.noGenresLabel.isHidden = false
                    } else {
                        cell.noGenresLabel.isHidden = true
                    }
                }
            } else {
                cell.noGenresLabel.isHidden = false
            }
            return cell
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "test", for: indexPath) as! LandingTableViewCell
                cell.label.text = "More Genres"
                cell.arrowImage.image = #imageLiteral(resourceName: "expandArrow")
                if genres == musicGenres.count {
                    cell.isSelected = true
                    cell.arrowImage.image = #imageLiteral(resourceName: "collapseArrow")
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "genres", for: indexPath) as! MoreGenresCell
                if selectedArray.contains(indexPath.row-1) {
                    
                    cell.isSelected = true
                    cell.selectedImageView.isHidden = false
                    tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                }
                
                cell.genreImageView.image = genreImages[indexPath.row-1]
                cell.nameLabel.text = musicGenres[indexPath.row-1]
                
                return cell
            }
        } else  {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "test", for: indexPath) as! LandingTableViewCell
                cell.label.text = "Rankings "
                cell.arrowImage.image = #imageLiteral(resourceName: "expandArrow")
                if rankings == users.count {
                    cell.isSelected = true
                    cell.arrowImage.image = #imageLiteral(resourceName: "collapseArrow")
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
                return cell
            } else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "rankingsCell", for: indexPath) as! RankingsTableViewCell
                cell.setupContent(rank: "\(indexPath.row)", name: users[indexPath.row - 1].username, points: users[indexPath.row - 1].points, index: indexPath.row - 1,flag : users[indexPath.row - 1].countryImage,profilePicture : users[indexPath.row - 1].profilePicture)
                let username = UserDefaults.standard.value(forKey: "username") as! String
                if users[indexPath.row - 1].username == username {
                    cell.profileImageView.layer.borderColor = MyVariables.pinkColor.cgColor
                }
                return cell
            }
        }
    }
}
