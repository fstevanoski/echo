//
//  MoreGenresCell.swift
//  Echo
//
//  Created by Pero on 6/16/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class MoreGenresCell: UITableViewCell {

    var genreImageView : UIImageView!
    var nameLabel : UILabel!
    var selectedImageView : UIImageView!
    var pointsLabel : UILabel!
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        selectedImageView.isHidden = true
    }
    
    func setupViews() {
        selectionStyle = .none
        genreImageView = UIImageView()
        genreImageView.image = #imageLiteral(resourceName: "rock_color")
        genreImageView.layer.masksToBounds = true
        genreImageView.layer.cornerRadius = 5
        genreImageView.contentMode = .scaleAspectFit
        
        nameLabel = UILabel()
        nameLabel.text = "ROCK"
        nameLabel.textColor = UIColor.black
        
        selectedImageView = UIImageView()
        selectedImageView.image = #imageLiteral(resourceName: "selected")
        selectedImageView.contentMode = .scaleAspectFit
        selectedImageView.isHidden = true
        
        pointsLabel = UILabel()
        pointsLabel.textColor = UIColor.black
        pointsLabel.isHidden = true
        pointsLabel.font = UIFont.systemFont(ofSize: 15)
        
        contentView.addSubview(genreImageView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(selectedImageView)
        contentView.addSubview(pointsLabel)
        
    }
    
    func setupConstraints () {
        genreImageView.snp.makeConstraints { make in
            make.left.equalTo(contentView).offset(10)
            make.width.height.equalTo(40)
            make.centerY.equalTo(self.snp.centerY)
        }
        nameLabel.snp.makeConstraints { make in
            make.left.equalTo(genreImageView.snp.right).offset(20)
            make.top.equalTo(contentView)
            make.width.equalTo(contentView.snp.width).dividedBy(2.1)
            make.height.equalTo(contentView)
        }
        selectedImageView.snp.makeConstraints { make in
            make.right.equalTo(contentView).offset(-10)
            make.width.height.equalTo(20)
            make.centerY.equalTo(self.snp.centerY)
        }
        pointsLabel.snp.makeConstraints { make in
            make.right.equalTo(contentView).offset(-10)
            make.centerY.equalTo(self.snp.centerY)
        }
       
    }
    
}
