//
//  LandingCollectionViewCell.swift
//  Echo
//
//  Created by Pero on 5/25/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class MusicGenresCollectionViewCell: UICollectionViewCell {
    
    var imageView : UIImageView!
    var titleGenre : UILabel!
    var blackandwhiteImages = [#imageLiteral(resourceName: "Rock_black&white"),#imageLiteral(resourceName: "pop_black&white"),#imageLiteral(resourceName: "techno_black&white"),#imageLiteral(resourceName: "jazz_black&white"),#imageLiteral(resourceName: "blues_black&white"),#imageLiteral(resourceName: "classical_black&white"),#imageLiteral(resourceName: "house_black&white"),#imageLiteral(resourceName: "metal_black&white")]
    var colorImages = [#imageLiteral(resourceName: "rock_color"),#imageLiteral(resourceName: "pop_color"),#imageLiteral(resourceName: "techno_color"),#imageLiteral(resourceName: "jazz"),#imageLiteral(resourceName: "blues_color"),#imageLiteral(resourceName: "classical_color"),#imageLiteral(resourceName: "house_color"),#imageLiteral(resourceName: "metal_color")]

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        contentView.backgroundColor = UIColor.white
        imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
        
        titleGenre = UILabel()
        titleGenre.backgroundColor = UIColor.clear
        titleGenre.font = MyVariables.fontBold20
        titleGenre.textColor = UIColor.white
        titleGenre.textAlignment = .left
        
        
        contentView.addSubview(imageView)
        contentView.addSubview(titleGenre)

    }
    
    
    
    func setupConstraints() {
        imageView.snp.makeConstraints { make in
            make.top.left.right.bottom.equalTo(contentView)
        }
        
        titleGenre.snp.makeConstraints { make in
            make.top.equalTo(self).offset(10)
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-10)
            make.height.equalTo(20)
        }
    }
}
