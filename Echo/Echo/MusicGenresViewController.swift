//
//  MusicGenresViewController.swift
//  Echo
//
//  Created by Pero on 5/26/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class MusicGenresViewController: UIViewController {
    
    var descriptionLabel : UILabel!
    
    var collectionView : UICollectionView!
    var selectedItems = 0
    var br = 0
    var selectedIndexes = [Int]()
    var saveButton : UIButton!
    var blackandwhiteImages = [#imageLiteral(resourceName: "Rock_black&white"),#imageLiteral(resourceName: "pop_black&white"),#imageLiteral(resourceName: "techno_black&white"),#imageLiteral(resourceName: "jazz_black&white"),#imageLiteral(resourceName: "blues_black&white"),#imageLiteral(resourceName: "classical_black&white"),#imageLiteral(resourceName: "house_black&white"),#imageLiteral(resourceName: "metal_black&white")]
    var colorImages = [#imageLiteral(resourceName: "rock_color"),#imageLiteral(resourceName: "pop_color"),#imageLiteral(resourceName: "techno_color"),#imageLiteral(resourceName: "jazz"),#imageLiteral(resourceName: "blues_color"),#imageLiteral(resourceName: "classical_color"),#imageLiteral(resourceName: "house_color"),#imageLiteral(resourceName: "metal_color")]
    var musicGenres = ["Rock","Pop","Techno","Jazz","Blues","Classical","House","Metal"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        
        navigationItem.backBarButtonItem?.tintColor = MyVariables.pinkColor
        if UserDefaults.standard.array(forKey: "selected") as? [Int] != nil {
          selectedIndexes =  UserDefaults.standard.array(forKey: "selected") as! [Int]
        }
        
        self.view.backgroundColor = UIColor.white
        self.title = "Choose 4 genres"
        
        descriptionLabel = UILabel()
        descriptionLabel.text = "Choose your 4 favourite genres!"
        descriptionLabel.textAlignment = .center
        descriptionLabel.isHidden = true
        
        collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: CustomFlowLayout())
        collectionView.register(MusicGenresCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.backgroundColor = UIColor.white
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.allowsMultipleSelection = true
        collectionView.bounces = false
        
        saveButton = UIButton()
        saveButton.setTitle("SAVE", for: .normal)
        saveButton.backgroundColor = UIColor.black
        saveButton.setTitleColor(UIColor.white, for: .normal)
        saveButton.addTarget(self, action: #selector(onCLickSave), for: .touchUpInside)
        
        self.view.addSubview(descriptionLabel)
        self.view.addSubview(collectionView)
        self.view.addSubview(saveButton)
    }
    
    func setupConstraints() {
        descriptionLabel.snp.makeConstraints { make in
            make.top.equalTo(self.view)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(self.view)
            make.height.equalTo(20)
        }
        saveButton.snp.makeConstraints { make in
            make.bottom.equalTo(self.view)
            make.width.equalTo(self.view)
            make.height.equalTo(self.view.snp.height).dividedBy(10)
        }
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(10)
            make.left.equalTo(self.view).offset(10)
            make.right.equalTo(self.view).offset(-10)
            make.bottom.equalTo(saveButton.snp.top).offset(-10)
        }
    }
    
    func onCLickSave() {
        UserDefaults.standard.set(selectedIndexes, forKey: "selected")
        _ = self.navigationController?.popViewController(animated: true)
    }
}

extension MusicGenresViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! MusicGenresCollectionViewCell
        if selectedIndexes.count < 4 {
            cell.imageView.image = colorImages[indexPath.row]
            
            selectedIndexes.append(indexPath.row)
        } else {
            cell.isSelected = false
            collectionView.deselectItem(at: indexPath, animated: false)
            let alert = UIAlertController(title: "", message: "You've reached your limit", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! MusicGenresCollectionViewCell
        cell.imageView.image = blackandwhiteImages[indexPath.row]
        selectedIndexes = selectedIndexes.filter {$0 != indexPath.row}
    }
}

extension MusicGenresViewController : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return blackandwhiteImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MusicGenresCollectionViewCell
        cell.titleGenre.text = musicGenres[indexPath.row]
        if selectedIndexes.contains(indexPath.row) {
            cell.imageView.image = colorImages[indexPath.row]
            cell.isSelected = true
            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .init(rawValue: 0))
        } else {
            cell.imageView.image = blackandwhiteImages[indexPath.row]
        }
        return cell
    }
    
}
