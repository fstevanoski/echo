//
//  MyVariables.swift
//  Echo
//
//  Created by Miki Dimitrov on 6/22/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import Foundation
import UIKit

struct MyVariables {
    static var pinkColor = UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 1)
    static var fontRegular30 = UIFont(name: "SanFranciscoDisplay-Regular", size: 30)
    static var fontRegular15 = UIFont(name: "SanFranciscoDisplay-Regular", size: 15)
    static var fontRegular20 = UIFont(name: "SanFranciscoDisplay-Regular", size: 20)
    static var fontBold15 = UIFont(name: "SanFranciscoDisplay-Bold", size: 15)
    static var fontBold20 = UIFont(name: "SanFranciscoDisplay-Bold", size: 20)
}
