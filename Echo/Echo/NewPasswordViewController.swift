//
//  NewPasswordViewController.swift
//  Echo
//
//  Created by Pero on 6/27/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class NewPasswordViewController: UIViewController {
    
    var oldPasswordlabel : UILabel!
    var oldPassword : UITextField!
    var newPasswordLabel : UITextField!
    var newPassword : UITextField!
    var confirmPasswordlabel : UILabel!
    var confirmPassword : UITextField!
    var changePasswordButton : UIButton!
    var cancelButton : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupViews() {
        self.view.backgroundColor = UIColor.white
        self.title = "Change Password"
        navigationItem.hidesBackButton = true
        oldPassword = UITextField()
        oldPassword.placeholder = "Current password"
        oldPassword.font = MyVariables.fontRegular15
        oldPassword.textAlignment = .center
        oldPassword.layer.borderColor = UIColor.lightGray.cgColor
        oldPassword.layer.borderWidth = 1
        oldPassword.layer.cornerRadius = 10
        oldPassword.isSecureTextEntry = true
        
        newPassword = UITextField()
        newPassword.placeholder = "New password"
        newPassword.font = MyVariables.fontRegular15
        newPassword.textAlignment = .center
        newPassword.layer.borderColor = UIColor.lightGray.cgColor
        newPassword.layer.borderWidth = 1
        newPassword.layer.cornerRadius = 10
        newPassword.isSecureTextEntry = true
        
        confirmPassword = UITextField()
        confirmPassword.placeholder = "Retype new password"
        confirmPassword.font = MyVariables.fontRegular15
        confirmPassword.textAlignment = .center
        confirmPassword.layer.borderColor = UIColor.lightGray.cgColor
        confirmPassword.layer.borderWidth = 1
        confirmPassword.layer.cornerRadius = 10
        confirmPassword.isSecureTextEntry = true
        
        changePasswordButton = UIButton()
        changePasswordButton.setTitle("Change Password", for: .normal)
        changePasswordButton.setTitleColor(UIColor.white, for: .normal)
        changePasswordButton.titleLabel?.font = MyVariables.fontBold20
        changePasswordButton.backgroundColor = MyVariables.pinkColor
        changePasswordButton.layer.cornerRadius = 10
        changePasswordButton.addTarget(self, action: #selector(changePassButtonTouched), for: .touchUpInside)
        
        cancelButton = UIButton()
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleColor(UIColor.white, for: .normal)
        cancelButton.titleLabel?.font = MyVariables.fontBold20
        cancelButton.backgroundColor = MyVariables.pinkColor
        cancelButton.layer.cornerRadius = 10
        cancelButton.addTarget(self, action: #selector(cancelButtonTouched), for: .touchUpInside)
        
        self.view.addSubview(oldPassword)
        self.view.addSubview(newPassword)
        self.view.addSubview(confirmPassword)
        self.view.addSubview(changePasswordButton)
        self.view.addSubview(cancelButton)
        
    }
    
    func setupConstraints() {
        oldPassword.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(20)
            make.left.equalTo(self.view).offset(15)
            make.right.equalTo(self.view).offset(-15)
            make.height.equalTo(self.view.snp.height).dividedBy(12)
            
        }
        newPassword.snp.makeConstraints { make in
            make.top.equalTo(oldPassword.snp.bottom).offset(20)
            make.left.equalTo(oldPassword)
            make.right.equalTo(oldPassword)
            make.height.equalTo(self.view.snp.height).dividedBy(12)
            
        }
        confirmPassword.snp.makeConstraints { make in
            make.top.equalTo(newPassword.snp.bottom).offset(3)
            make.left.equalTo(oldPassword)
            make.right.equalTo(oldPassword)
            make.height.equalTo(self.view.snp.height).dividedBy(12)
        }
        changePasswordButton.snp.makeConstraints { make in
            make.top.equalTo(confirmPassword.snp.bottom).offset(15)
            make.left.equalTo(oldPassword)
            make.right.equalTo(oldPassword)
            make.height.equalTo(self.view.snp.height).dividedBy(10)
            
        }
        cancelButton.snp.makeConstraints { make in
            make.top.equalTo(changePasswordButton.snp.bottom).offset(10)
            make.left.equalTo(oldPassword)
            make.right.equalTo(oldPassword)
            make.height.equalTo(self.view.snp.height).dividedBy(10)
        }
        
    }
    
    func changePassButtonTouched() {
        let password = UserDefaults.standard.string(forKey: "password")
        if (oldPassword.text == "" ) || (newPassword.text == "") || (confirmPassword.text == "") {
            let alert = UIAlertController(title: "", message: "Enter text in the fields", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        if isValidPassword(pass: newPassword.text!) == false {
            let alert = UIAlertController(title: "", message: "Password must be atleast 8 characters long", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)

        }
        if (newPassword.text != confirmPassword.text) || (oldPassword.text != password) {
            let alert = UIAlertController(title: "", message: "Passwords must match", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
        UserDefaults.standard.set(confirmPassword.text, forKey: "password")
       _ = navigationController?.popViewController(animated: true)
        }
    }
    
    func isValidPassword(pass:String) -> Bool {
        let passRegEx = ".{8,}"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        return passwordTest.evaluate(with: pass)
    }
    
    func cancelButtonTouched() {
        _ = navigationController?.popViewController(animated: true)
        
    }


}
