//
//  NotificationTableViewCell.swift
//  Echo
//
//  Created by Pero on 6/23/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource {

    var pickerTextField : UITextField!
    var pickerView = UIPickerView()
    var pickerArray = [["00","01","02","03","04","05","06","07","08","09","10","11","13","14","15","16","17","18","19","20","21","22","23"],["00","01","02","03","04","05","06","07","08","09","10","11","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"]]
    var label : UILabel!
    

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupViews() {
       
        label = UILabel()
        label.font = MyVariables.fontRegular15
        label.textAlignment = .center
        label.textColor = UIColor.black
        
        pickerView.delegate = self

        contentView.addSubview(label)
        
    }
    
    func setupConstraints() {
        label.snp.makeConstraints { make in
            make.edges.equalTo(contentView)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return pickerArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray[component].count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[component][row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let hours = pickerArray[0][pickerView.selectedRow(inComponent: 0)]
        let minutes = pickerArray[1][pickerView.selectedRow(inComponent: 1)]
        pickerTextField.text = hours + ":" + minutes
    }
}

extension NotificationTableViewCell : UITextFieldDelegate {
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if label.text == "From" {
//            UserDefaults.standard.set(pickerTextField.text, forKey: "from")
//            let a = UserDefaults.standard.string(forKey: "from")
//            print(a!)
//        } else if label.text == "To" {
//            UserDefaults.standard.set(pickerTextField.text, forKey: "to")
//            let b = UserDefaults.standard.string(forKey: "to")
//            print(b!)
//        }
//    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if label.text == "From" {
            print(pickerTextField.text!)
          UserDefaults.standard.set(pickerTextField.text, forKey: "from")

        } else if label.text == "To" {
             print(pickerTextField.text!)
            UserDefaults.standard.set(pickerTextField.text, forKey: "to")

        }
    }
}

