//
//  NotificationTimeViewController.swift
//  Echo
//
//  Created by Pero on 6/22/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit
import SnapKit

class NotificationTimeViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{

    var tableView : UITableView!
    var pickerView = UIPickerView()
    var pickerArray = [["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"],["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"]]
    var selectedIndexPath : IndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints() 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupViews() {
        self.view.backgroundColor = UIColor.white
        navigationController?.navigationBar.tintColor = MyVariables.pinkColor
        tableView = UITableView()
        tableView.register(NotificationTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
       
        pickerView.delegate = self
        pickerView.dataSource = self
        
        pickerView.selectRow(2400, inComponent: 0, animated: false)
        pickerView.selectRow(6000, inComponent: 1, animated: false)
        self.view.addSubview(pickerView)
        self.view.addSubview(tableView)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(60)
            make.left.right.equalTo(self.view)
            make.height.equalTo(80)
        }
        
        pickerView.snp.makeConstraints { make in
           make.left.right.bottom.equalTo(self.view)
           make.height.equalTo(200)
        }
    
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return pickerArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 18000
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return pickerArray[component][row%24]
        } else {
            return pickerArray[component][row%60]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let hours = pickerArray[0][pickerView.selectedRow(inComponent: 0)%24]
        let minutes = pickerArray[1][pickerView.selectedRow(inComponent: 1)%60]
        let cell = tableView.cellForRow(at: selectedIndexPath) as! NotificationTableViewCell
        if selectedIndexPath.row == 0 {
            cell.label.text = "From " + hours + " : " + minutes
            UserDefaults.standard.set(cell.label.text, forKey: "from")
        } else {
            cell.label.text = "To " + hours + " : " + minutes
            UserDefaults.standard.set(cell.label.text, forKey: "to")
        }
    }

}

extension NotificationTimeViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
    }
}

extension NotificationTimeViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NotificationTableViewCell
        if indexPath.row == 0 {
            selectedIndexPath = indexPath
            cell.label.text = "From"
            cell.isSelected = true
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        } else if indexPath.row == 1 {
            cell.label.text = "To"
        }
        return cell
    }
    
}
 
