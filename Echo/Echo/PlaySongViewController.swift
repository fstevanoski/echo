//
//  PlaySongViewController.swift
//  Echo
//
//  Created by Pero on 6/19/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox
import MediaPlayer

class PlaySongViewController: UIViewController {
    
    var imageView : UIImageView!
    var player : AVAudioPlayer!
    var artistNameLabel : UILabel!
    var songNameLabel : UILabel!
    var slider : UISlider!
    
    var currentTimeLabel : UILabel!
    var remainingTimeLabel : UILabel!
    var previousSongButton : UIButton!
    var playpauseButton : UIButton!
    var nextSongButton : UIButton!
    var volumeControll : UISlider!
    var minVolumeImageView : UIImageView!
    var maxVolumeImageView : UIImageView!
    var volumee : MPVolumeView!
    var leftHeaderButton : UIButton!
    var counter = UserDefaults.standard.integer(forKey: "counter")
    var songsList = ["tntLong","despasitoLong","bethovenLong","chuckLong"]
    var songNames = ["TNT","Despasito","Fur Elise","Johhny B. Goode"]
    var artistNames = ["AC/DC","Louis Fonsi","Beethoven","Chuck Berry"]
    var songImages = [#imageLiteral(resourceName: "acdc_new1"),#imageLiteral(resourceName: "despasitoNotification"),#imageLiteral(resourceName: "beethovenNot.jpg"),#imageLiteral(resourceName: "chuckNotification.jpg")]
    let comandCenter = MPRemoteCommandCenter.shared()
    var counterPlay = 0
    var correctAnswerList = [Int]()
    var indexSet : IndexSet!
    var songlist1 = [String]()
    var songNames1 = [String]()
    var artistNames1 = [String]()
    var songImages1 = [UIImage]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.beginReceivingRemoteControlEvents()
        playSound()
        setupView()
        setupConstraints()
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        player.stop()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.tintColor = MyVariables.pinkColor
        navigationController?.navigationBar.isTranslucent = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.isTranslucent = false
        
    }
    
    func setupView() {
        
        if UserDefaults.standard.array(forKey: "correctSongs") as? [Int] != nil {
            correctAnswerList = UserDefaults.standard.array(forKey: "correctSongs") as! [Int]
        }
        
        indexSet = IndexSet(correctAnswerList)
        songlist1 = indexSet.map { songsList[$0] }
        artistNames1 = indexSet.map { artistNames[$0] }
        songNames1 = indexSet.map { songNames[$0] }
        songImages1 = indexSet.map { songImages[$0] }
        //let songsList1 = Set(songsList.index).intersection(Set(correctAnswerList))
        print(songlist1)
        navigationController?.navigationBar.isTranslucent = false
        self.view.backgroundColor = UIColor.white
        let songName = UserDefaults.standard.string(forKey: "song")
        self.title = songName!
        imageView = UIImageView()
        let imageData = UserDefaults.standard.data(forKey: "artistImage")
        imageView.image = UIImage(data: imageData!)
        imageView.contentMode = .scaleToFill
        
        slider = UISlider()
        slider.maximumValue = Float(player.duration)
        slider.setThumbImage(#imageLiteral(resourceName: "circle"), for: .normal)
        slider.addTarget(self, action: #selector(adjustDuration), for: .valueChanged)
        
        currentTimeLabel = UILabel()
        currentTimeLabel.textColor = UIColor.gray
        currentTimeLabel.font = UIFont.systemFont(ofSize: 12)
        remainingTimeLabel = UILabel()
        remainingTimeLabel.textColor = UIColor.gray
        remainingTimeLabel.font = UIFont.systemFont(ofSize: 12)
        
        artistNameLabel = UILabel()
        artistNameLabel.text = UserDefaults.standard.string(forKey: "artist")
        artistNameLabel.textColor = UIColor.black
        artistNameLabel.textAlignment = .center
        artistNameLabel.font = UIFont.boldSystemFont(ofSize: 25)
        
        songNameLabel = UILabel()
        songNameLabel.text = UserDefaults.standard.string(forKey: "song")
        songNameLabel.textColor = MyVariables.pinkColor
        songNameLabel.textAlignment = .center
        
        previousSongButton = UIButton()
        previousSongButton.setImage(#imageLiteral(resourceName: "rewind"), for: .normal)
        previousSongButton.addTarget(self, action: #selector(previousSongButtonTouched), for: .touchUpInside)
        
        playpauseButton = UIButton()
        playpauseButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
        playpauseButton.addTarget(self, action: #selector(challenge), for: .touchUpInside)
        
        nextSongButton = UIButton()
        nextSongButton.setImage(#imageLiteral(resourceName: "forward"), for: .normal)
        nextSongButton.addTarget(self, action: #selector(nextSongButtonTouched), for: .touchUpInside)
        
        minVolumeImageView = UIImageView()
        minVolumeImageView.image = #imageLiteral(resourceName: "min_Volume")
        maxVolumeImageView = UIImageView()
        maxVolumeImageView.image = #imageLiteral(resourceName: "max_Volume")
        
        volumeControll = UISlider()
        volumeControll.value = 0.5
        volumeControll.setThumbImage(#imageLiteral(resourceName: "circle"), for: .normal)
        volumeControll.addTarget(self, action: #selector(volumeChanged), for: .valueChanged)
        
        //        commandCenter.pauseCommand.isEnabled = true
        //        commandCenter.pauseCommand.addTarget(self, action: #selector(challenge))
        //        commandCenter.pauseCommand.addTarget {event in
        //            self.player.pause()
        //            return .success
        //        }

        comandCenter.skipForwardCommand.isEnabled = false
        comandCenter.skipBackwardCommand.isEnabled = false
        comandCenter.nextTrackCommand.isEnabled = true
        comandCenter.nextTrackCommand.addTarget(self, action: #selector(nextSongButtonTouched))
        
        comandCenter.pauseCommand.isEnabled = true
        comandCenter.pauseCommand.addTarget(self, action: #selector(lockScreenPause))
        comandCenter.playCommand.isEnabled = true
        comandCenter.playCommand.addTarget(self, action: #selector(lockScreenPlay))
        comandCenter.previousTrackCommand.isEnabled = true
        comandCenter.previousTrackCommand.addTarget(self, action: #selector(previousSongButtonTouched))
     //   comandCenter.changePlaybackPositionCommand.addTarget(, action: <#T##Selector#>)
        

        
        self.view.addSubview(slider)
        self.view.addSubview(imageView)
        self.view.addSubview(currentTimeLabel)
        self.view.addSubview(remainingTimeLabel)
        self.view.addSubview(artistNameLabel)
        self.view.addSubview(songNameLabel)
        self.view.addSubview(previousSongButton)
        self.view.addSubview(nextSongButton)
        self.view.addSubview(playpauseButton)
        self.view.addSubview(minVolumeImageView)
        self.view.addSubview(maxVolumeImageView)
        self.view.addSubview(volumeControll)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints { make in
            make.top.left.equalTo(self.view).offset(15)
            make.right.equalTo(self.view).offset(-15)
            make.height.equalTo(self.view.snp.height).dividedBy(2)
        }
        slider.snp.makeConstraints { make in
            make.top.equalTo(imageView.snp.bottom).offset(10)
            make.left.equalTo(imageView)
            make.right.equalTo(imageView)
            make.height.equalTo(20)
        }
        currentTimeLabel.snp.makeConstraints { make in
            make.left.equalTo(self.view).offset(10)
            make.top.equalTo(slider.snp.bottom)
            make.width.equalTo(50)
            make.height.equalTo(10)
        }
        remainingTimeLabel.snp.makeConstraints { make in
            make.right.equalTo(self.view).offset(-10)
            make.top.equalTo(slider.snp.bottom)
            make.height.equalTo(currentTimeLabel)
            make.width.equalTo(currentTimeLabel)
        }
        artistNameLabel.snp.makeConstraints { make in
            make.top.equalTo(currentTimeLabel.snp.bottom).offset(10)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.view.snp.height).dividedBy(14)
        }
        songNameLabel.snp.makeConstraints { make in
            make.top.equalTo(artistNameLabel.snp.bottom)
            make.centerX.equalTo(artistNameLabel)
            make.width.equalTo(self.view)
            make.height.equalTo(artistNameLabel)
        }
        playpauseButton.snp.makeConstraints { make in
            make.top.equalTo(songNameLabel.snp.bottom).offset(10)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(40)
            make.height.equalTo(40)
        }
        previousSongButton.snp.makeConstraints { make in
            make.top.equalTo(playpauseButton)
            make.right.equalTo(playpauseButton.snp.left).offset(-50)
            make.width.equalTo(playpauseButton)
            make.height.equalTo(playpauseButton)
        }
        nextSongButton.snp.makeConstraints { make in
            make.top.equalTo(playpauseButton)
            make.left.equalTo(playpauseButton.snp.right).offset(50)
            make.width.equalTo(playpauseButton)
            make.height.equalTo(playpauseButton)
        }
        volumeControll.snp.makeConstraints { make in
            make.top.equalTo(playpauseButton.snp.bottom).offset(40)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(self.view.snp.width).dividedBy(1.5)
            make.height.equalTo(10)
            
        }
        minVolumeImageView.snp.makeConstraints { make in
            make.top.equalTo(volumeControll)
            make.right.equalTo(volumeControll.snp.left).offset(-5)
            make.width.equalTo(10)
            make.height.equalTo(volumeControll)
            
        }
        maxVolumeImageView.snp.makeConstraints { make in
            make.top.equalTo(volumeControll)
            make.left.equalTo(volumeControll.snp.right).offset(5)
            make.width.equalTo(10)
            make.height.equalTo(volumeControll)
        }
    }
    
//    override func remoteControlReceived(with event: UIEvent?) {
//        if let event = event {
//            if event.type == .remoteControl {
//                switch event.subtype {
//                case .remoteControlNextTrack:
//                    nextSongButtonTouched()
//                case .remoteControlPreviousTrack:
//                    previousSongButtonTouched()
//                default:
//                    print("")
//                }
//            }
//        }
//    }
    
    func lockScreenNextSong() {
    
    }
    
    func lockScreenPause() {
        player.pause()
    }
    func lockScreenPlay() {
        player.play()
    }
    
    func nextSongButtonTouched() {
        
          if counter < songlist1.count-1 {
            counter += 1
            let url = Bundle.main.url(forResource: songlist1[counter], withExtension: "mp3")!
           

            artistNameLabel.text = artistNames1[counter]
            songNameLabel.text = songNames1[counter]
            imageView.image = songImages1[counter]
            self.title = songNameLabel.text!
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                player = try AVAudioPlayer(contentsOf: url)
                guard let player = player else { return }
                player.prepareToPlay()
                player.volume = volumeControll.value
                player.play()
                
                let image = imageView.image
                let size = CGSize(width: 100, height: 100)
                let artwork = MPMediaItemArtwork(boundsSize: size, requestHandler: { (size) -> UIImage in
                    return image!
                })
                
                MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyArtist : artistNameLabel.text!, MPMediaItemPropertyTitle :  songNameLabel.text!, MPMediaItemPropertyPlaybackDuration : player.duration, MPMediaItemPropertyArtwork : artwork]
                becomeFirstResponder()
    

            } catch let error as NSError {
                print(error.description)
            }
          }
    }
    
    func previousSongButtonTouched() {
        
        if counter > 0 {
            
            counter -= 1
            let url = Bundle.main.url(forResource: songlist1[counter], withExtension: "mp3")!
            
            artistNameLabel.text = artistNames1[counter]
            songNameLabel.text = songNames1[counter]
            imageView.image = songImages1[counter]
            self.title = songNameLabel.text!
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                player = try AVAudioPlayer(contentsOf: url)
                guard let player = player else { return }
                player.prepareToPlay()
                player.volume = volumeControll.value
                player.play()

                let image = imageView.image
                let size = CGSize(width: 100, height: 100)
                let artwork = MPMediaItemArtwork(boundsSize: size, requestHandler: { (size) -> UIImage in
                    return image!
                })

                
                //  let artwork = MPMediaItemArtwork(boundsSize: CGSize(dictionaryRepresentation: self.view.frame as! CFDictionary)!, requestHandler: image)
                MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyArtist : artistNameLabel.text!, MPMediaItemPropertyTitle :  songNameLabel.text!, MPMediaItemPropertyPlaybackDuration : player.duration, MPMediaItemPropertyArtwork : artwork]
                becomeFirstResponder()

                
            } catch let error as NSError {
                print(error.description)
            }
        }
    }
    
    
    func playSound() {
         let playSong = UserDefaults.standard.string(forKey: "playSong")
        let url = Bundle.main.url(forResource: playSong, withExtension: "mp3")!
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
           // player.prepareToPlay()
            player.play()
            
            let artist = UserDefaults.standard.string(forKey: "artist")
            let song = UserDefaults.standard.string(forKey: "song")
            let imageData = UserDefaults.standard.data(forKey: "artistImage")

            let image = UIImage(data: imageData!)
            let size = CGSize(width: 100, height: 100)
            let artwork = MPMediaItemArtwork(boundsSize: size, requestHandler: { (size) -> UIImage in
                return image!
            })
            //  let artwork = MPMediaItemArtwork(boundsSize: CGSize(dictionaryRepresentation: self.view.frame as! CFDictionary)!, requestHandler: image)
            
            MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyArtist : artist!, MPMediaItemPropertyTitle : song!, MPMediaItemPropertyPlaybackDuration : player.duration, MPMediaItemPropertyArtwork : artwork]
             becomeFirstResponder()
            

        } catch let error as NSError {
            print(error.description)
        }
    }
    
    
    func updateTime() {
        
        slider.value = Float(player.currentTime)
        currentTimeLabel.text = time(seconds: Int(slider.value))
        
        let remaining = Float(player.duration) - slider.value
        remainingTimeLabel.text = time(seconds: Int(remaining))
        
    }
    
    func volumeChanged() {
        player.volume = volumeControll.value
    }
    
    func adjustDuration() {
        
        player.currentTime = TimeInterval(slider.value)
        slider.value = Float(player.currentTime)
        let m = time(seconds: Int(slider.value))
        currentTimeLabel.text = m
        player.play()
    }
    
    func challenge() {
       counterPlay += 1
        if counterPlay % 2 == 1 {
            player.pause()
            playpauseButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        } else {
            player.play()
            playpauseButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
            
        }
    }
    
    func time (seconds : Int) -> (String) {
        let minutes = (seconds % 3600) / 60
        let seconds = (seconds % 3600) % 60
        return String(format:"%02i:%02i",  minutes, seconds)
        // return ("\((seconds % 3600) / 60):\((seconds % 3600) % 60)")
    }
    
    
}
