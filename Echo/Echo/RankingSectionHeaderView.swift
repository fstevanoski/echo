//
//  RankingSectionHeaderView.swift
//  Echo
//
//  Created by Miki Dimitrov on 6/14/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import Foundation
import UIKit

class RankingSectionHeaderView : UIView {
    
    
    var rankLabel : UILabel!
    var nameLabel : UILabel!
    var pointsLabel : UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        self.backgroundColor = UIColor.white
        rankLabel = createLabel(text: "RANK")
        nameLabel = createLabel(text: "USERNAME")
        pointsLabel = createLabel(text: "POINTS")
        pointsLabel.textAlignment = .center
        
        self.addSubview(rankLabel)
        self.addSubview(nameLabel)
        self.addSubview(pointsLabel)
    }
    
    func createLabel(text : String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 10)
        return label
    }
    
    func setupConstraints() {
        rankLabel.snp.makeConstraints { make in
            make.left.equalTo(self).offset(5)
            make.top.bottom.equalTo(self)
            make.width.equalTo(50)
        }
        nameLabel.snp.makeConstraints { make in
            make.left.equalTo(rankLabel.snp.right)
            make.top.bottom.equalTo(self)
            make.width.equalTo(self.snp.width).multipliedBy(0.4)
        }
        pointsLabel.snp.makeConstraints { make in
            make.left.equalTo(nameLabel.snp.right)
            make.top.bottom.equalTo(self)
            make.right.equalTo(self)
        }
        
    }
    
}
