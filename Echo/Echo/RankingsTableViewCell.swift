//
//  RankingsTableViewCell.swift
//  Echo
//
//  Created by Miki Dimitrov on 6/14/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import Foundation
import UIKit

class RankingsTableViewCell : UITableViewCell {
    
    var rankingLabel : UILabel!
    var profileImageView : UIImageView!
    var nameLabel : UILabel!
    var pointsLabel : UILabel!
    var countryFlagImageView : UIImageView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        profileImageView.layer.borderColor = UIColor.clear.cgColor
    }
    
    func setupViews() {
        selectionStyle = .blue
        rankingLabel = createLabel(text: "1")
        rankingLabel.textAlignment = .right
        let username = UserDefaults.standard.value(forKey: "username") as! String
        nameLabel = createLabel(text: username)
        nameLabel.font = UIFont.boldSystemFont(ofSize: 15)
        
        pointsLabel = createLabel(text: "100")
        
        profileImageView = UIImageView()
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.layer.masksToBounds = true
        profileImageView.layer.cornerRadius = 15
        profileImageView.layer.borderColor = UIColor.clear.cgColor
        profileImageView.layer.borderWidth = 3
        
        countryFlagImageView = UIImageView()
        countryFlagImageView.contentMode = .scaleAspectFit
        
        contentView.addSubview(rankingLabel)
        contentView.addSubview(nameLabel)
        contentView.addSubview(pointsLabel)
        contentView.addSubview(profileImageView)
        contentView.addSubview(countryFlagImageView)
    }
    
    func setupContent(rank : String,name : String,points : Int,index : Int,flag : UIImage,profilePicture : UIImage) {
        
        rankingLabel.text = rank
        nameLabel.text = name
        pointsLabel.text = "\(points)"
        profileImageView.image = profilePicture
        countryFlagImageView.image = flag
        if index % 2 == 0{
            contentView.backgroundColor = UIColor.white
        } else {
            contentView.backgroundColor = UIColor.init(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        }

    }
    
    func createLabel(text : String) -> UILabel {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.text = text
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = UIColor.black
        label.textAlignment = .left
        return label
    }
    
    func setupConstraints() {
        rankingLabel.snp.makeConstraints { make in
           make.left.equalTo(contentView)
           make.top.bottom.equalTo(contentView)
           make.width.equalTo(20)
        }
        profileImageView.snp.makeConstraints { make in
           make.left.equalTo(rankingLabel.snp.right).offset(5)
           make.top.equalTo(contentView).offset(1)
           make.bottom.equalTo(contentView).offset(-1)
           make.width.equalTo(30)
        }
        nameLabel.snp.makeConstraints { make in
           make.left.equalTo(profileImageView.snp.right).offset(3)
           make.top.bottom.equalTo(contentView)
           make.width.equalTo(contentView.snp.width).multipliedBy(0.5)
        }
        pointsLabel.snp.makeConstraints { make in
            make.left.equalTo(nameLabel.snp.right).offset(3)
            make.top.bottom.equalTo(contentView)
            make.width.equalTo(40)
        }
        countryFlagImageView.snp.makeConstraints { make in
            make.left.equalTo(pointsLabel.snp.right).offset(3)
            make.centerY.equalTo(contentView)
            make.width.equalTo(30)
            make.height.equalTo(20)
        }
    }
}
