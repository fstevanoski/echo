//
//  RankingsTableViewCell.swift
//  Echo
//
//  Created by Miki Dimitrov on 6/14/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import Foundation
import UIKit

enum Mode {
  case Global
  case Country
}


class RankingsViewController : UIViewController {
    
    var points = 0
    var tableView : UITableView!
    var segmentedControl : UISegmentedControl!
    var users = [User]()
    var usersByCountry = [User]()
    var user : User!
    var mode = Mode.Global
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        
    }
    
    func setupViews() {
        self.title = "Rankings"
        
        self.view.backgroundColor = UIColor.white
        tableView = UITableView()
        tableView.register(RankingsTableViewCell.self, forCellReuseIdentifier: "rankingsCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = UIColor.clear
        
        let items = ["GLOBAL","COUNTRY"]
        segmentedControl = UISegmentedControl(items : items)
        segmentedControl.backgroundColor = UIColor.white
        segmentedControl.tintColor = MyVariables.pinkColor
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(segmentedControlChangedValue), for: .valueChanged)
        
        
        self.view.addSubview(segmentedControl)
        self.view.addSubview(tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initializeUser()
        loadData()
        for i in 0...users.count - 1 {
            if users[i].username == user.username {
               users[i] = user
            }
        }
    }
    func setupConstraints() {
        
        segmentedControl.snp.makeConstraints { make in
            make.left.equalTo(self.view).offset(30)
            make.right.equalTo(self.view).offset(-30)
            make.top.equalTo(self.view).offset(20)
            make.height.equalTo(30)
        }
        tableView.snp.makeConstraints { make in
           make.top.equalTo(segmentedControl.snp.bottom).offset(20)
           make.right.left.bottom.equalTo(self.view)
        }
    }
    
    func initializeUser() {
        var profilePicture : UIImage!
        let username = UserDefaults.standard.value(forKey: "username") as! String
        
        if  UserDefaults.standard.value(forKey: "points") as? Int != nil {
             points = UserDefaults.standard.value(forKey: "points") as! Int
        }
        
        
        if  let profilePictureData = UserDefaults.standard.value(forKey: "image") as? Data {
            profilePicture = UIImage(data: profilePictureData)
        } else {
            profilePicture = #imageLiteral(resourceName: "user")
        }
        
        user = User(username: username, points: points, profilePicture: profilePicture!, countryImage: #imageLiteral(resourceName: "macedonianFlag"), country: "Macedonia")
    }
    
    func loadData() {
        var names = ["D1B","Mohammed Azhar","Filip Stevanoski","Sechkov Pero","Komal","Ravi","Eldin","Miki","Dejan","Roben","Gaetan le Singe","Pept","Trajche","Aden"]
        var points = [60,517,33,267,204,191,17,172,1333,1602,15,122,120,119]
        var profilePictures = [#imageLiteral(resourceName: "user1"),#imageLiteral(resourceName: "user2"),#imageLiteral(resourceName: "user3"),#imageLiteral(resourceName: "user4"),#imageLiteral(resourceName: "user5"),#imageLiteral(resourceName: "user6"),#imageLiteral(resourceName: "user7"),#imageLiteral(resourceName: "user8"),#imageLiteral(resourceName: "user9"),#imageLiteral(resourceName: "user10"),#imageLiteral(resourceName: "user2"),#imageLiteral(resourceName: "user4"),#imageLiteral(resourceName: "user10"),#imageLiteral(resourceName: "user9")]
        var countryImages = [#imageLiteral(resourceName: "americanFlag"),#imageLiteral(resourceName: "macedonianFlag"),#imageLiteral(resourceName: "mexicanFlag"),#imageLiteral(resourceName: "serbianFlag"),#imageLiteral(resourceName: "bosnianFlag"),#imageLiteral(resourceName: "croatianFlag")]
        var countries = ["United States","Macedonia","Mexico","Serbia","Bosnia and Herzegovina","Croatia"]
        for i in 0...13 {
            users.append(User(username: names[i], points: points[i], profilePicture: profilePictures[i], countryImage: countryImages[i%6],country : countries[i%6]))
        }
        users.append(user)
        
        users.sort() {
            $0.0.points > $0.1.points
        }
    }
    
    func segmentedControlChangedValue() {
        if segmentedControl.selectedSegmentIndex == 0 {
            mode = Mode.Global
            usersByCountry = [User]()
            tableView.reloadData()
        } else {
            mode = Mode.Country
            for user in users {
                if user.country == "Macedonia" {
                   usersByCountry.append(user)
                }
            }
            tableView.reloadData()
        }
    }
}

extension RankingsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 32
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = RankingSectionHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
        return header
    }
   
}

extension RankingsViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if mode == .Global {
            return users.count
        } else {
            return usersByCountry.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rankingsCell", for: indexPath) as! RankingsTableViewCell
        if mode == .Global {
            cell.setupContent(rank: "\(indexPath.row + 1)", name: users[indexPath.row].username, points: users[indexPath.row].points, index: indexPath.row,flag : users[indexPath.row].countryImage,profilePicture : users[indexPath.row].profilePicture)
        } else {
           cell.setupContent(rank: "\(indexPath.row + 1)", name: usersByCountry[indexPath.row].username, points: usersByCountry[indexPath.row].points, index: indexPath.row,flag : usersByCountry[indexPath.row].countryImage,profilePicture : usersByCountry[indexPath.row].profilePicture)
        }
        return cell
    }
}
