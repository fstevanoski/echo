//
//  SecondActivationScreenFooterView.swift
//  Echo
//
//  Created by Pero on 5/23/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class SecondActivationScreenFooterView: UIView {
    
    var continueButton : UIButton!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupContraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        continueButton = UIButton()
        continueButton.setTitle("Continue", for: .normal)
        continueButton.setTitleColor(UIColor.white, for: .normal)
        continueButton.backgroundColor = MyVariables.pinkColor
        continueButton.layer.cornerRadius = 15
        // continueButton.addTarget(self, action: #selector(click), for: .touchUpInside)
        self.addSubview(continueButton)
    }
    
    func setupContraints() {
        continueButton.snp.makeConstraints { make in
            make.left.top.equalTo(self).offset(20)
            make.right.equalTo(self).offset(-20)
            make.bottom.equalTo(self).offset(-10)
            
        }
        
    }
    
    
}
