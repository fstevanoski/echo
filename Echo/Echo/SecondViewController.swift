//
//  SecondViewController.swift
//  Echo
//
//  Created by Pero on 5/22/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var tableView : UITableView!
    var infoArray = ["E-mail","Password","Username","Music Genres"]
    //var colorImages = [#imageLiteral(resourceName: "rock_color"),#imageLiteral(resourceName: "pop_color"),#imageLiteral(resourceName: "techno_color"),#imageLiteral(resourceName: "jazz"),#imageLiteral(resourceName: "blues_color"),#imageLiteral(resourceName: "classical_color"),#imageLiteral(resourceName: "house_color"),#imageLiteral(resourceName: "metal_color")]
    var musicGenres = ["Rock","Pop","Techno","Jazz","Blues","Classical","House","Metal"]
    let imagePicker = UIImagePickerController()
    var header : HeaderViewSecondActivationScreen!
    var resultArray : [String]!
    var continueButton : UIButton!
    let swiftColor = UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 1)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.backgroundColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func setupView() {
        
        self.view.backgroundColor = UIColor.white
        let tapRecognizer = UITapGestureRecognizer(target: self, action:#selector(imageTapped(tapGestureRecognizer:)))
       
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UserInfoTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.bounces = true
        tableView.tableFooterView = UIView()
        
        header = HeaderViewSecondActivationScreen(frame : CGRect(x: 0, y: 0, width: self.view.frame.width, height: 200))
        tableView.tableHeaderView = header
        header.userImageView.addGestureRecognizer(tapRecognizer)
        
        continueButton = UIButton()
        continueButton.setTitle("Continue", for: .normal)
        continueButton.setTitleColor(UIColor.white, for: .normal)
        continueButton.backgroundColor = MyVariables.pinkColor
        continueButton.layer.cornerRadius = 15
        continueButton.addTarget(self, action: #selector(onClickContinue), for: .touchUpInside)
        
        self.view.addSubview(tableView)
        self.view.addSubview(continueButton)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.top.equalTo(self.view)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(continueButton.snp.top).offset(-10)
        }
        continueButton.snp.makeConstraints { make in
            make.bottom.equalTo(self.view).offset(-30)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(self.view.snp.width).dividedBy(1.1)
            make.height.equalTo(self.view.snp.height).dividedBy(10)
        }
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    func onClickContinue() {
        
        let email = UserDefaults.standard.string(forKey: "email")
        let password = UserDefaults.standard.string(forKey: "password")
        let username = UserDefaults.standard.string(forKey: "username")
        if (email != nil && email != "") && (password != nil && password != "") && (username != nil && username != "") {
            if isValidEmail(email!) == false  {
                let alert = UIAlertController(title: "", message: "Email is not valid", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else if isValidPassword(pass: password!) == false {
                let alert = UIAlertController(title: "", message: "Password must be atleast 8 characters long", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
               self.navigationController?.pushViewController(ThirdActivationScreenController(), animated: true)
            }
        } else {
            let alert = UIAlertController(title: "", message: "Enter text in the required fields", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            header.userImageView.image = image
            UserDefaults.standard.set(UIImagePNGRepresentation(image), forKey: "image")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func isValidPassword(pass:String) -> Bool {
        let passRegEx = ".{8,}"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        return passwordTest.evaluate(with: pass)
    }
    func isValidEmail(_ testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

extension SecondViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3 {
            navigationController?.pushViewController(MusicGenresViewController(), animated: true)
        }
    }
}

extension SecondViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UserInfoTableViewCell
       
        cell.delegate = self
        cell.selectionStyle = .none
        cell.infoLabel.text = infoArray[indexPath.row]
        
        if indexPath.row == 1 {
            cell.infoTextField.isSecureTextEntry = true
        }
        if indexPath.row == 3 {
            cell.infoTextField.isHidden = true
            cell.musicLabel.isHidden = false
          if let selectedArray = UserDefaults.standard.array(forKey: "selected") {
            let indexSet = selectedArray as! [Int]
            resultArray = indexSet.map { musicGenres[$0] }
            cell.infoTextField.isUserInteractionEnabled = false
            let string = resultArray.joined(separator: ", ")
            cell.musicLabel.text = string
            cell.musicLabel.textColor = UIColor.black
            print(string)
            }
        }

        switch indexPath.row {
        case 0:
            cell.editInfo(key: "email")
        case 1:
            cell.editInfo(key: "password")
        case 2:
            cell.editInfo(key: "username")
        case 3:
           cell.editInfo(key: "selected")
        default:
            print("")
        }
   
        if indexPath.row < 4 {
            cell.infoTextField.placeholder = "Required"
        }
        return cell
    }
}

extension SecondViewController : UserInfoTableViewCellDelegate {
    func changeConstraints() {
        tableView.snp.remakeConstraints { remake in
            remake.top.equalTo(self.view).offset(-80)
            remake.left.right.bottom.equalTo(self.view)
        }
    }
    func remakeConstraints() {
        tableView.snp.remakeConstraints { remake in
            remake.edges.equalTo(self.view)
        }
    }
    
}
