//
//  SignUpViewContoller.swift
//  Echo
//
//  Created by Pero on 6/21/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import FacebookShare
import TwitterKit
import TwitterCore
import FBSDKCoreKit

class SignUpViewContoller: UIViewController {
    
    var logoImageView : UIImageView!
    var descriptionLabel : UILabel!
    var createAccountButton : UIButton!
    var fbButton : UIButton!
    var twitterButton : UIButton!
    var label1 : UILabel!
    var label2 : UILabel!
    var twitterLogo : UIImageView!
    var fbLogo : UIImageView!
    var sigInLabel : UILabel!
    var sigInButton : UIButton!
    let swiftColor = UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupContraints()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupViews() {
        
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.backgroundColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = swiftColor
        self.view.backgroundColor = UIColor.white
        logoImageView = UIImageView()
        logoImageView.image = #imageLiteral(resourceName: "logoAppIcon")
        logoImageView.contentMode = .scaleAspectFit
        
        descriptionLabel = UILabel()
        descriptionLabel.numberOfLines = 0
        descriptionLabel.font = UIFont(name: "SanFranciscoDisplay-Regular", size: 20)
        descriptionLabel.text = "It's time to compete\nwith your friends"
        descriptionLabel.textColor = UIColor.black
        descriptionLabel.textAlignment = .center
        
        createAccountButton = UIButton()
        createAccountButton.setTitle("CREATE AN ACCOUNT", for: .normal)
        createAccountButton.titleLabel?.font = UIFont(name: "SanFranciscoDisplay-Bold", size: 15)
        createAccountButton.setTitleColor(UIColor.white, for: .normal)
        createAccountButton.backgroundColor = swiftColor
        createAccountButton.layer.cornerRadius = 5
        createAccountButton.addTarget(self, action: #selector(createAccountTouched), for: .touchUpInside)
        
        label1 = UILabel()
        label1.backgroundColor = UIColor.lightGray
        label2 = UILabel()
        label2.backgroundColor = UIColor.lightGray
        
        fbButton = UIButton()
        fbButton.layer.cornerRadius = 5
        fbButton.layer.masksToBounds = true
        fbButton.backgroundColor = UIColor.init(red: 77/255, green: 108/255, blue: 239/255, alpha: 1)
        fbButton.addTarget(self, action: #selector(fbButtonTouched), for: .touchUpInside)
        
        twitterButton = UIButton()
        twitterButton.layer.cornerRadius = 5
        twitterButton.layer.masksToBounds = true
        twitterButton.backgroundColor = UIColor.init(red: 70/255, green: 154/255, blue: 233/255, alpha: 1)
        twitterButton.addTarget(self, action: #selector(twitterButtonTouched), for: .touchUpInside)
        
        twitterLogo = UIImageView()
        twitterLogo.image = #imageLiteral(resourceName: "twitterIcon")
        twitterLogo.contentMode = .scaleAspectFit
        
        fbLogo = UIImageView()
        fbLogo.image = #imageLiteral(resourceName: "facebook_Icon_Small")
        fbLogo.contentMode = .scaleAspectFit
        
        sigInLabel = UILabel()
        sigInLabel.text = "ALREADY HAVE AN ACCOUNT?"
        sigInLabel.font = UIFont(name: "SanFranciscoDisplay-Bold", size: 10)
        sigInLabel.textAlignment = .right
        
        sigInButton = UIButton()
        sigInButton.setTitle("Sign In", for: .normal)
        sigInButton.contentHorizontalAlignment = .left
        sigInButton.setTitleColor(MyVariables.pinkColor, for: .normal)
        sigInButton.titleLabel?.font = UIFont(name: "SanFranciscoDisplay-Bold", size: 13)
        sigInButton.backgroundColor = UIColor.white
        
        self.view.addSubview(logoImageView)
        self.view.addSubview(descriptionLabel)
        self.view.addSubview(createAccountButton)
        self.view.addSubview(label1)
        self.view.addSubview(fbButton)
        self.view.addSubview(twitterButton)
        self.view.addSubview(twitterLogo)
        self.view.addSubview(fbLogo)
        self.view.addSubview(label2)
        self.view.addSubview(sigInLabel)
        self.view.addSubview(sigInButton)
    }
    
    func setupContraints() {
        
        let x = self.view.frame.size.width/5
        let size = (self.view.frame.size.width - 36)/2
        logoImageView.snp.makeConstraints { make in
            make.bottom.equalTo(descriptionLabel.snp.top).offset(-50)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(x)
            make.height.equalTo(x)
        }
        descriptionLabel.snp.makeConstraints { make in
            make.bottom.equalTo(createAccountButton.snp.top).offset(-50)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(self.view.snp.width).dividedBy(1.5)
            make.height.equalTo(self.view.snp.height).dividedBy(9)
            
        }
        createAccountButton.snp.makeConstraints { make in
            make.bottom.equalTo(label1.snp.top).offset(-60)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(self.descriptionLabel.snp.width)
            make.height.equalTo(self.view.snp.height).dividedBy(12)
            
        }
        label1.snp.makeConstraints { make in
            make.bottom.equalTo(twitterButton.snp.top).offset(-15)
            make.left.right.equalTo(self.view)
            make.height.equalTo(1)
            
        }
        
        fbButton.snp.makeConstraints { make in
            make.top.equalTo(twitterButton)
            make.left.equalTo(self.view).offset(12)
            make.width.equalTo(size)
            make.height.equalTo(createAccountButton)
        }
        twitterButton.snp.makeConstraints { make in
            make.bottom.equalTo(label2.snp.top).offset(-15)
            make.left.equalTo(fbButton.snp.right).offset(12)
            make.width.equalTo(fbButton)
            make.height.equalTo(fbButton)
        }
        twitterLogo.snp.makeConstraints { make in
            make.center.equalTo(twitterButton.snp.center)
            make.height.equalTo(25)
            make.width.equalTo(25)
        }
        fbLogo.snp.makeConstraints { make in
            make.center.equalTo(fbButton.snp.center)
            make.height.equalTo(30)
            make.width.equalTo(30)
            
        }
        label2.snp.makeConstraints { make in
            make.bottom.equalTo(sigInLabel.snp.top).offset(-30)
            make.left.right.equalTo(self.view)
            make.height.equalTo(1)
        }
        sigInLabel.snp.makeConstraints { make in
            make.bottom.equalTo(sigInButton)
            make.left.equalTo(self.view).offset(45)
            make.width.equalTo(self.view.snp.width).dividedBy(2)
            make.height.equalTo(20)
            
        }
        sigInButton.snp.makeConstraints { make in
            make.bottom.equalTo(self.view).offset(-30)
            make.left.equalTo(sigInLabel.snp.right).offset(10)
            make.width.equalTo(sigInLabel)
            make.height.equalTo(20)
        }
        
        
    }
    
    func fbButtonTouched() {
        
        let loginManager = LoginManager()
        loginManager.logIn([.publicProfile,.email], viewController: self, completion: {  (result) in
            switch result {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, let accessToken):
                let authToken = accessToken.authenticationToken
                let userId = accessToken.userId!
                print(authToken)
                print(userId)
                
                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "email, name, gender, picture.type(large)"]).start(completionHandler: { (connection, result, error) -> Void in
                    if (error == nil){
                        let fbDetails = result as! [String : Any]
                        let userName = fbDetails["name"] as! String
                        let email = fbDetails["email"] as! String
                        let pictureData = fbDetails["picture"] as! [String : Any]
                        let data = pictureData["data"] as! [String : Any]
                        let pictureUrl = data["url"] as! String
                        let pictureUrlData = NSData(contentsOf: URL(string : pictureUrl)!)
                        let gender = fbDetails["gender"] as! String
                        //print(location)
                        UserDefaults.standard.set(gender, forKey: "sex")
                        UserDefaults.standard.set(pictureUrlData, forKey: "image")
                        UserDefaults.standard.set(email, forKey: "email")
                        UserDefaults.standard.set(userName, forKey: "username")
                        UserDefaults.standard.set(true, forKey: "social")
                        UserDefaults.standard.set(1, forKey: "activationFinish")
                        let nav1 = UINavigationController()
                        nav1.pushViewController(LandingViewController(), animated: false)
                        self.navigationController?.present(nav1, animated: true, completion: nil)
                    }
                })
            }
        })
        
    }
    
    func twitterButtonTouched() {
        
//                let store = Twitter.sharedInstance().sessionStore
//                if let userID = store.session()?.userID {
//                    store.logOutUserID(userID)
//                    print("Log out")
//                }
        Twitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                
                print("signed in as \(session?.userName)");
                let userName = session?.userName
                let client = TWTRAPIClient.withCurrentUser()
                
                client.requestEmail { email, error in
                    if (email != nil) {
//                        UserDefaults.standard.set(userName, forKey: "username")
//                        UserDefaults.standard.set(email, forKey: "email")
//                        UserDefaults.standard.set(1, forKey: "activationFinish")
//                        let nav1 = UINavigationController()
//                        nav1.pushViewController(LandingViewController(), animated: false)
//                        self.navigationController?.present(nav1, animated: true, completion: nil)
                        print(email!)
                    } else {
                        print("error: \(error?.localizedDescription)");
                    }
                }
                
                UserDefaults.standard.set(userName, forKey: "username")
                //UserDefaults.standard.set(email, forKey: "email")
                UserDefaults.standard.set(true, forKey: "social")
                UserDefaults.standard.set(1, forKey: "activationFinish")
                let nav1 = UINavigationController()
                nav1.pushViewController(LandingViewController(), animated: false)
                self.navigationController?.present(nav1, animated: true, completion: nil)
                
                
            } else {
                print("error: \(error?.localizedDescription)");
            }
        })
    }
    
    func createAccountTouched() {
        self.navigationController?.pushViewController(SecondViewController(), animated: true)
        
    }
    
}
