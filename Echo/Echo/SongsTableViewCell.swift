//
//  SongsTableViewCell.swift
//  Echo
//
//  Created by Pero on 6/21/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class SongsTableViewCell: UITableViewCell {

    var genreImageView : UIImageView!
    var songNameLabel : UILabel!
    var artistNameLabel : UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupContraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        selectionStyle = .none
        genreImageView = UIImageView()
        genreImageView.image = #imageLiteral(resourceName: "rock_color")
        genreImageView.layer.masksToBounds = true
        genreImageView.layer.cornerRadius = 5
        genreImageView.contentMode = .scaleToFill
        
        songNameLabel = UILabel()
        songNameLabel.textColor = UIColor.black
        
        artistNameLabel = UILabel()
        artistNameLabel.textColor = UIColor.lightGray
      
        contentView.addSubview(genreImageView)
        contentView.addSubview(songNameLabel)
        contentView.addSubview(artistNameLabel)
    }
    
    func setupContraints() {
        genreImageView.snp.makeConstraints { make in
            make.left.equalTo(contentView).offset(10)
            make.width.height.equalTo(40)
            make.centerY.equalTo(self.snp.centerY)
        }
        songNameLabel.snp.makeConstraints { make in
            make.left.equalTo(genreImageView.snp.right).offset(20)
            make.top.equalTo(contentView)
            make.width.equalTo(contentView)
            make.height.equalTo(contentView.snp.height).dividedBy(2)
        }
        artistNameLabel.snp.makeConstraints { make in
            make.left.equalTo(genreImageView.snp.right).offset(20)
            make.top.equalTo(songNameLabel.snp.bottom)
            make.height.equalTo(songNameLabel)
            make.width.equalTo(songNameLabel)
            
        }
    }

}

