//
//  SongsViewController.swift
//  Echo
//
//  Created by Pero on 6/19/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class SongsViewController: UIViewController {

    var tableView : UITableView!
    var songNames = ["TNT","Despasito","Fur Elise","Johhny B. Goode"]
    //var songNames = [String]()
    var artistNames = ["AC/DC","Luis Fonsi","Ludwig van Bethoven","Chuck Berry"]
    var songImages = [#imageLiteral(resourceName: "acdc_new1"),#imageLiteral(resourceName: "despasitoNotification"),#imageLiteral(resourceName: "beethovenNot.jpg"),#imageLiteral(resourceName: "chuckNotification.jpg")]
    var indexSet = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()

    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.array(forKey: "correctSongs") as? [Int] != nil {
            indexSet =  UserDefaults.standard.array(forKey: "correctSongs") as! [Int]
           
        }
        
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func setupView() {
        self.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.title = "Playlist"
        
        tableView = UITableView()
        tableView.register(SongsTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        
        self.view.addSubview(tableView)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.edges.equalTo(self.view)
        }
    }
}

extension SongsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! SongsTableViewCell
        switch cell.songNameLabel.text! {
        case "TNT":
            UserDefaults.standard.set("tntLong", forKey: "playSong")
            UserDefaults.standard.set(indexPath.row, forKey: "counter")
        case "Despasito":
            UserDefaults.standard.set("despasitoLong", forKey: "playSong")
            UserDefaults.standard.set(indexPath.row, forKey: "counter")
        case "Fur Elise":
            UserDefaults.standard.set("bethovenLong", forKey: "playSong")
            UserDefaults.standard.set(indexPath.row, forKey: "counter")
        case "Johhny B. Goode":
            UserDefaults.standard.set("chuckLong", forKey: "playSong")
            UserDefaults.standard.set(indexPath.row, forKey: "counter")
        default:
            print("")
        }
        
        UserDefaults.standard.set(UIImagePNGRepresentation(cell.genreImageView.image!), forKey: "artistImage")
        UserDefaults.standard.set(cell.songNameLabel.text, forKey: "song")
        UserDefaults.standard.set(cell.artistNameLabel.text, forKey: "artist")
        self.navigationController?.pushViewController(PlaySongViewController(), animated: true)
    }
}

extension SongsViewController : UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return indexSet.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SongsTableViewCell
        cell.genreImageView.image = songImages[indexSet[indexPath.row]]
        cell.songNameLabel.text = songNames[indexSet[indexPath.row]]
        cell.artistNameLabel.text = artistNames[indexSet[indexPath.row]]
        return cell
    }
}
