//
//  ThirdActivationScreenController.swift
//  Echo
//
//  Created by Pero on 5/23/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class ThirdActivationScreenController: UIViewController {

    var logoImageView : UIImageView!
    var welcomeLabel : UILabel!
    var nextButton : UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupView() {
        self.navigationController?.navigationBar.isHidden = false
        self.view.backgroundColor = UIColor.white
        //self.navigationItem.hidesBackButton = true
        logoImageView = UIImageView()
        logoImageView.image = #imageLiteral(resourceName: "logoAppIcon")
        logoImageView.layer.masksToBounds = true
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.layer.cornerRadius = 10
        
        welcomeLabel = UILabel()
        welcomeLabel.text = "Welcome to Echo"
        welcomeLabel.font = MyVariables.fontRegular20
        welcomeLabel.numberOfLines = 0
        welcomeLabel.textAlignment = .center
        welcomeLabel.textColor = UIColor.black
        
        nextButton = UIButton()
        nextButton.setTitle("Get Started", for: .normal)
        nextButton.setTitleColor(UIColor.white, for: .normal)
        nextButton.backgroundColor = MyVariables.pinkColor
        nextButton.titleLabel?.font = UIFont(name: "SanFransiscoDisplay-Bold", size: 20)
        nextButton.titleLabel?.textAlignment = .center
        nextButton.layer.cornerRadius = 15
        nextButton.addTarget(self, action: #selector(click), for: .touchUpInside)
        
        self.view.addSubview(logoImageView)
        self.view.addSubview(welcomeLabel)
        self.view.addSubview(nextButton)
        
    }
    
    func setupConstraints() {
        let x = self.view.frame.width/5
        logoImageView.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(120)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(self.view.snp.width).dividedBy(4)
            make.height.equalTo(x)
        }
        welcomeLabel.snp.makeConstraints { make in
            make.top.equalTo(logoImageView.snp.bottom).offset(10)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(self.view.snp.width)
        }
        nextButton.snp.makeConstraints { make in
            make.bottom.equalTo(self.view).offset(-30)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(self.view.snp.width).dividedBy(1.1)
            make.height.equalTo(self.view.snp.height).dividedBy(10)
        }
    }
    
    func click () {
        UserDefaults.standard.set(1, forKey: "activationFinish")
        let nav1 = UINavigationController()
        nav1.pushViewController(LandingViewController(), animated: false)
        self.navigationController?.present(nav1, animated: true, completion: nil)
    }
}
