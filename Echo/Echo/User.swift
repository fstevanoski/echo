//
//  User.swift
//  Echo
//
//  Created by Miki Dimitrov on 6/14/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import Foundation
import UIKit

class User  {
 
    var username : String!
    var points : Int!
    var profilePicture : UIImage!
    var countryImage : UIImage!
    var country : String!
    
    init(username : String,points : Int,profilePicture : UIImage,countryImage : UIImage,country : String) {
        self.username = username
        self.points = points
        self.profilePicture = profilePicture
        self.countryImage = countryImage
        self.country = country
    }

}
