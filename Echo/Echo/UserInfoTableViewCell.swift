//
//  UserInfoTableViewCell.swift
//  Echo
//
//  Created by Pero on 5/22/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

protocol UserInfoTableViewCellDelegate {
    func changeConstraints()
    func remakeConstraints()
}

class UserInfoTableViewCell: UITableViewCell, UIPickerViewDelegate {
    
    var delegate : UserInfoTableViewCellDelegate?
    let datepicker = UIDatePicker()
    let sexpicker = ["Not Set","Male","Female"]
    var notificationTimeArray = ["00:00 - 00:59","01:00 - 01:59","02:00 - 02:59","03:00 - 03:59","04:00 - 04:59","05:00 - 05:59","06:00 - 06:59","07:00 - 07:59","08:00 - 08:59","09:00 - 09:59","10:00 - 10:59","11:00 - 12:59","12:00 - 12:59","13:00 - 13:59","14:00 - 14:59","15:00 - 15:59","16:00 - 16:59","17:00 - 17:59","18:00 - 18:59","19:00 - 19:59","20:00 - 20:59","21:00 - 21:59","22:00 - 22:59","23:00 - 23:59"]
    var infoLabel : UILabel!
    let sexPickerView = UIPickerView()
    let countryPickerView = UIPickerView()
    let notificationTimePickerView = UIPickerView()
    var infoTextField : UITextField!
    var musicLabel : UILabel!
    var toolBar : UIToolbar!
    let countriesArray = ["United States", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Indian Ocean Territory", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma (Myanmar)", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Cook Islands", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Democratic Republic of the Congo", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "North Korea", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Republic of the Congo", "Romania", "Russia", "Rwanda", "Saint Barthelemy", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Martin", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "US Virgin Islands", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"]
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        sexPickerView.delegate = self
        sexPickerView.tintColor = UIColor.black
        sexPickerView.backgroundColor = UIColor.white
        sexPickerView.tag = 3
        
        countryPickerView.delegate = self
        countryPickerView.tintColor = UIColor.black
        countryPickerView.backgroundColor = UIColor.white
        countryPickerView.tag = 2
        
        notificationTimePickerView.delegate = self
        notificationTimePickerView.tintColor = UIColor.black
        notificationTimePickerView.backgroundColor = UIColor.white
        notificationTimePickerView.tag = 1
        
        toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        toolBar.barTintColor = UIColor.white
        datepicker.backgroundColor = UIColor.white
        
        let doneButton = UIBarButtonItem(title: "DONE", style: .done, target: self, action: #selector(onClickDoneButton))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
       // let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.bordered, target: self, action: nil)
        toolBar.setItems([spaceButton,doneButton], animated: true)
        
        infoLabel = UILabel()
        infoLabel.textColor = UIColor.black
        infoLabel.font = MyVariables.fontBold20
        infoLabel.textAlignment = .left
        
        infoTextField = UITextField()
        infoTextField.textColor = UIColor.black
        infoTextField.font = MyVariables.fontRegular15
        infoTextField.autocorrectionType = .no
        infoTextField.delegate = self
        
        musicLabel = UILabel()
        musicLabel.textAlignment = .left
        musicLabel.text = "Optional"
        musicLabel.font = MyVariables.fontRegular15
        musicLabel.textColor = UIColor.lightGray
        musicLabel.lineBreakMode = .byWordWrapping
        musicLabel.numberOfLines = 0
        musicLabel.isHidden = true
        
        contentView.addSubview(infoLabel)
        contentView.addSubview(infoTextField)
        contentView.addSubview(musicLabel)
    }
    
    func setupConstraints() {
        infoLabel.snp.makeConstraints { make in
            make.top.equalTo(contentView)
            make.left.equalTo(contentView).offset(15)
            make.width.equalTo(contentView.snp.width).dividedBy(2)
            make.height.equalTo(contentView)
        }
        infoTextField.snp.makeConstraints { make in
            make.top.equalTo(contentView)
            make.left.equalTo(infoLabel.snp.right)
            make.right.equalTo(contentView).offset(-5)
            make.height.equalTo(infoLabel)
        }
        musicLabel.snp.makeConstraints { make in
            make.top.equalTo(contentView)
            make.left.equalTo(infoLabel.snp.right)
            make.right.equalTo(contentView).offset(-5)
            make.height.equalTo(infoLabel)
        }
    }
    func editInfo (key : String) {
        if let text = UserDefaults.standard.string(forKey: key) {
            infoTextField.text = text
        } else {
            infoTextField.text=""
        }
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    
        if pickerView == sexPickerView {
            return sexpicker.count
        } else if pickerView == countryPickerView {
            return countriesArray.count
        } else {
          return notificationTimeArray.count
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == sexPickerView {
            return sexpicker[row]
        } else if pickerView == countryPickerView {
            return countriesArray[row]
        } else {
            return notificationTimeArray[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == sexPickerView {
           infoTextField.text =  sexpicker[row]
        } else if pickerView == countryPickerView {
            infoTextField.text = countriesArray[row]
        } else  {
            infoTextField.text = notificationTimeArray[row]
        }
    }
    
    func onClickDoneButton() {

        infoTextField.resignFirstResponder()
    }
}


extension UserInfoTableViewCell : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if infoLabel.text == "Date of Birth" {
            delegate?.changeConstraints() 
        datepicker.datePickerMode = UIDatePickerMode.date
        textField.inputView = datepicker
            infoTextField.inputAccessoryView = toolBar
        datepicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
            
        } else if infoLabel.text == "Sex" {
            infoTextField.inputView = sexPickerView
            infoTextField.inputAccessoryView = toolBar
            delegate?.changeConstraints()
        } else if infoLabel.text == "Country" {
            infoTextField.inputView = countryPickerView
            infoTextField.inputAccessoryView = toolBar
            delegate?.changeConstraints()
        } else if infoLabel.text == "Notification Time" {
            infoTextField.inputView = notificationTimePickerView
            infoTextField.inputAccessoryView = toolBar
        }
    }
    
    func datePickerValueChanged() {
        let dateFormat = DateFormatter()
        dateFormat.dateStyle = DateFormatter.Style.medium
        dateFormat.timeStyle = DateFormatter.Style.none
       
        infoTextField.text = dateFormat.string(from: datepicker.date)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var key = ""
        if infoLabel.text == "E-mail" {
            key = "email"
        } else if infoLabel.text == "Password" {
            key = "password"
        } else if infoLabel.text == "Username" {
            key = "username"
        } else {
            key = "music"
        }
        UserDefaults.standard.set(textField.text, forKey: key)
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if infoLabel.text == "Date of Birth" || infoLabel.text == "Sex"   {
            delegate?.remakeConstraints()
        }
        
        var key = ""
        if infoLabel.text == "E-mail" {
            key = "email"
        } else if infoLabel.text == "Password" {
            key = "password"
        } else if infoLabel.text == "Username" {
            key = "username"
        } else {
            key = "music"
        }
        
        UserDefaults.standard.set(textField.text, forKey: key)
        textField.resignFirstResponder()

    }
}
