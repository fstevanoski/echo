//
//  UserInfoViewController.swift
//  Echo
//
//  Created by Pero on 5/25/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class UserInfoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var closeButton : UIButton!
    var editButton : UIButton!
    var tableView : UITableView!
    var infoArray = ["E-mail","Password","Username","Date of Birth","Sex","Music Genres","Country","Quiet Hours"]
    var infoArraySocial = ["E-mail","Username","Date of Birth","Sex","Music Genres","Country","Quiet Hours"]
    var counter = 0
    let imagePicker = UIImagePickerController()
    var leftHeaderButton : UIButton!
    var rightHeaderButton : UIButton!
    var header : EditUserInfoHeaderView!
    var cell = EditUserInfoCell()
    var musicGenres = ["Rock","Pop","Techno","Jazz","Blues","Classical","House","Metal"]
    var resultArray = [String]()
    var disturb : String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.array(forKey: "selected") as? [Int] != nil {
            let selectedIndexes =  UserDefaults.standard.array(forKey: "selected") as! [Int]
            resultArray = selectedIndexes.map { musicGenres[$0] }
        }
        tableView.reloadData()
    }
    func setupView() {
        
        self.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = MyVariables.pinkColor
        
        leftHeaderButton = UIButton()
        leftHeaderButton.setTitle("Close", for: .normal)
        leftHeaderButton.setTitleColor(MyVariables.pinkColor, for: .normal)
        leftHeaderButton.addTarget(self, action: #selector(onClickCloseButton), for: .touchUpInside)
        leftHeaderButton.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        let leftbarButton = UIBarButtonItem(customView: leftHeaderButton)
        navigationItem.leftBarButtonItem = leftbarButton
        self.navigationItem.hidesBackButton = false
        
        rightHeaderButton = UIButton()
        rightHeaderButton.setTitle("Edit", for: .normal)
        rightHeaderButton.setTitleColor(MyVariables.pinkColor, for: .normal)
        rightHeaderButton.addTarget(self, action: #selector(onClickEdit), for: .touchUpInside)
        rightHeaderButton.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        let rightbarButton = UIBarButtonItem(customView: rightHeaderButton)
        navigationItem.rightBarButtonItem = rightbarButton
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action:#selector(imageTapped(tapGestureRecognizer:)))
        
        tableView = UITableView()
        tableView.register(EditUserInfoCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
        header = EditUserInfoHeaderView(frame : CGRect(x: 0, y: 0, width: self.view.frame.width, height: 140))
        tableView.tableHeaderView = header
        header.userImageView.addGestureRecognizer(tapRecognizer)
        
        self.view.addSubview(tableView)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.edges.equalTo(self.view)
        }
    }
    
    func onClickCloseButton() {
        let email = UserDefaults.standard.string(forKey: "email")
        //  let password = UserDefaults.standard.string(forKey: "password")
        let username = UserDefaults.standard.string(forKey: "username")
        if (email != nil && email != "")  && (username != nil && username != ""){
            if counter % 2 == 0 {
                self.dismiss(animated: true, completion: nil) }
        } else {
            let alert = UIAlertController(title: "", message: "Enter text in the required fields", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func onClickEdit(sender : UIButton) {
        counter += 1
        if counter % 2 == 1 {
            rightHeaderButton.setTitle("Done", for: .normal)
            tableView.allowsSelection = true
            header.userImageView.isUserInteractionEnabled = true
            header.myInfoLabel.isHidden = true
            tableView.reloadData()
            leftHeaderButton.isHidden = true
        } else if counter % 2 == 0 {
            rightHeaderButton.setTitle("Edit", for: .normal)
            tableView.allowsSelection = false
            leftHeaderButton.isHidden = false
            header.userImageView.isUserInteractionEnabled = false
            header.myInfoLabel.isHidden = false
            tableView.reloadData()
        }
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            header.userImageView.image = image
            UserDefaults.standard.set(UIImagePNGRepresentation(image), forKey: "image")
        } else {
            print("aaa")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension UserInfoViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if UserDefaults.standard.bool(forKey: "social") {
            let cell = tableView.cellForRow(at: indexPath) as! EditUserInfoCell
            cell.delegate = self
            if counter%2 == 1 {
                
                if indexPath.row == 1 {
                    cell.editInfoTextField.isUserInteractionEnabled = false
                } else {
                    cell.editInfoTextField.isUserInteractionEnabled = true
                }
                if indexPath.row == 4 {
                    navigationController?.pushViewController(MusicGenresViewController(), animated: true)
                } else if indexPath.row == 6 {
                    navigationController?.pushViewController(NotificationTimeViewController(), animated: true)
                }
            } else {
                cell.editInfoTextField.isUserInteractionEnabled = false
            }
            tableView.reloadData()
        } else {
            let cell = tableView.cellForRow(at: indexPath) as! EditUserInfoCell
            cell.delegate = self
            if counter%2 == 1 {
                if indexPath.row == 1 {
                    navigationController?.pushViewController(NewPasswordViewController(), animated: true)
                }
                if indexPath.row == 2 {
                    cell.editInfoTextField.isUserInteractionEnabled = false
                } else {
                    cell.editInfoTextField.isUserInteractionEnabled = true
                }
                if indexPath.row == 5 {
                    navigationController?.pushViewController(MusicGenresViewController(), animated: true)
                } else if indexPath.row == 7 {
                    navigationController?.pushViewController(NotificationTimeViewController(), animated: true)
                }
            } else {
                cell.editInfoTextField.isUserInteractionEnabled = false
            }
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 || indexPath.row == 1 {
            if counter%2 == 0 {
                return 0
            }
            return 55
        } else {
            return 55
        }
    }
}

extension UserInfoViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserDefaults.standard.bool(forKey: "social") {
            return infoArraySocial.count
        } else {
            return infoArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! EditUserInfoCell
        cell.infoLabel.text = infoArray[indexPath.row]
        
        if UserDefaults.standard.bool(forKey: "social") {
            if indexPath.row == 0 {
                cell.editInfoTextField.isHidden = true
                cell.label.isHidden = false
                cell.label.textColor = UIColor.black
                cell.label.text = UserDefaults.standard.string(forKey: "email")
            }
            if indexPath.row == 4 {
                cell.editInfoTextField.isHidden = true
                cell.label.isHidden = false
                cell.editInfoTextField.isUserInteractionEnabled = false
                let string = resultArray.joined(separator: ", ")
                cell.label.text = string
                cell.label.textColor = UIColor.black
            } else if indexPath.row == 6 {
                cell.editInfoTextField.isHidden = true
                cell.label.isHidden = false
                if UserDefaults.standard.value(forKey: "from") != nil && UserDefaults.standard.value(forKey: "to") != nil {
                    let from = UserDefaults.standard.string(forKey: "from")
                    let to = UserDefaults.standard.string(forKey: "to")
                    cell.label.textColor = UIColor.black
                    cell.label.text =  from! + "\n" + to!
                } else {
                    cell.label.textColor = UIColor.lightGray
                    cell.label.text = "Not Set >"
                }
            }
            
            switch indexPath.row {
            case 0:
                if counter%2 == 0 {
                    cell.editInfoTextField.text = ""
                } else {
                    cell.editInfo(key: "email")
                }
            case 1:
                if counter%2 == 0 {
                    cell.editInfoTextField.text = ""
                } else {
                    cell.editInfo(key: "username")
                }
            case 2:
                cell.editInfo(key: "date")
            case 3:
                cell.editInfo(key: "sex")
            case 4:
                cell.editInfo(key: "selected")
            case 5:
                cell.editInfo(key: "country")
            case 6:
                cell.editInfo(key: "disturb")
            default:
                print("")
            }
         return cell
       
        } else {
            if indexPath.row == 0 {
                cell.editInfoTextField.isHidden = true
                cell.label.isHidden = false
                cell.label.textColor = UIColor.black
                cell.label.text = UserDefaults.standard.string(forKey: "email")
            }
            if indexPath.row == 1 {
                cell.editInfoTextField.isHidden = true
                cell.label.isHidden = false
                cell.label.text = "Change Password >"
            }
            if indexPath.row == 5 {
                cell.editInfoTextField.isHidden = true
                cell.label.isHidden = false
                cell.editInfoTextField.isUserInteractionEnabled = false
                let string = resultArray.joined(separator: ", ")
                cell.label.text = string
                cell.label.textColor = UIColor.black
            } else if indexPath.row == 7 {
                cell.editInfoTextField.isHidden = true
                cell.label.isHidden = false
                if UserDefaults.standard.value(forKey: "from") != nil && UserDefaults.standard.value(forKey: "to") != nil {
                    let from = UserDefaults.standard.string(forKey: "from")
                    let to = UserDefaults.standard.string(forKey: "to")
                    cell.label.textColor = UIColor.black
                    cell.label.text =  from! + "\n" + to!
                } else {
                    cell.label.textColor = UIColor.lightGray
                    cell.label.text = "Not Set >"
                }
            }
            
            switch indexPath.row {
            case 0:
                if counter%2 == 0 {
                    cell.editInfoTextField.text = ""
                } else {
                    cell.editInfo(key: "email")
                }
            case 1:
                if counter%2 == 0 {
                    cell.editInfoTextField.text = ""
                } else {
                    cell.editInfo(key: "password")
                }
            case 2:
                cell.editInfo(key: "username")
            case 3:
                cell.editInfo(key: "date")
            case 4:
                cell.editInfo(key: "sex")
            case 5:
                cell.editInfo(key: "selected")
            case 6:
                cell.editInfo(key: "country")
            case 7:
                cell.editInfo(key: "disturb")
            default:
                print("")
            }
            return cell
        }
        
        }
    
}

extension UserInfoViewController : EditUserCellDelegate {
    func changeConstraints(value : Int) {
        tableView.snp.updateConstraints { make in
            make.top.equalTo(self.view).offset(value)
        }
    }
    
    func backToNormal() {
        tableView.snp.updateConstraints { make in
            make.top.equalTo(self.view)
        }
    }
}
