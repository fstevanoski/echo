//
//  UserProfileSectionHeaderView.swift
//  Echo
//
//  Created by Miki Dimitrov on 6/16/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import Foundation
import UIKit

class UserProfileSectionHeaderView : UIView {
    
    
    var genreLabel : UILabel!
    var pointsLabel : UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        self.backgroundColor = UIColor.white
        genreLabel = createLabel(text: "GENRE")
        genreLabel.textAlignment = .left
        pointsLabel = createLabel(text: "POINTS")
        pointsLabel.textAlignment = .right
        
        self.addSubview(genreLabel)
        self.addSubview(pointsLabel)
    }
    
    func createLabel(text : String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 10)
        return label
    }
    
    func setupConstraints() {
       
        genreLabel.snp.makeConstraints { make in
            make.left.equalTo(self).offset(60)
            make.top.bottom.equalTo(self)
            make.width.equalTo(self.snp.width).multipliedBy(0.4)
        }
        pointsLabel.snp.makeConstraints { make in
            make.top.bottom.equalTo(self)
            make.right.equalTo(self).offset(-10)
        }
        
    }
    
}
