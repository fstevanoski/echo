//
//  UserProfileViewController .swift
//  Echo
//
//  Created by Miki Dimitrov on 6/16/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import Foundation
import UIKit

class UserProfileViewController : UIViewController {
    
    
    var profilePictureImageView : UIImageView!
    var usernameLabel : UILabel!
    var tableView : UITableView!
    
    var musicGenres = ["Rock","Pop","Techno","Jazz","Blues","Classical","House","Metal"]
    var colorImages = [#imageLiteral(resourceName: "rock_color"),#imageLiteral(resourceName: "pop_color"),#imageLiteral(resourceName: "techno_color"),#imageLiteral(resourceName: "jazz"),#imageLiteral(resourceName: "blues_color"),#imageLiteral(resourceName: "classical_color"),#imageLiteral(resourceName: "house_color"),#imageLiteral(resourceName: "metal_color")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
    }
    
    func setupViews() {
        self.title = "Profile"
        self.view.backgroundColor = UIColor.white
        
        profilePictureImageView = UIImageView()
        profilePictureImageView.contentMode = .scaleToFill
        if let imageData = UserDefaults.standard.value(forKey: "image") as? Data {
            profilePictureImageView.image = UIImage(data: imageData)
        } else {
            profilePictureImageView.image = #imageLiteral(resourceName: "user")
        }
        profilePictureImageView.layer.masksToBounds = true
        profilePictureImageView.layer.cornerRadius = 50
        
        usernameLabel = UILabel()
        usernameLabel.text = "Filip Stevanoski"
        usernameLabel.textColor = UIColor.black
        usernameLabel.textAlignment = .center
        usernameLabel.font = UIFont.systemFont(ofSize: 20)
        
        tableView = UITableView()
        tableView.register(MoreGenresCell.self, forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelection = true
        
        self.view.addSubview(profilePictureImageView)
        self.view.addSubview(usernameLabel)
        self.view.addSubview(tableView)
    }
    
    func setupConstraints() {
        profilePictureImageView.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(30)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.height.equalTo(100)
        }
        usernameLabel.snp.makeConstraints { make in
            make.left.right.equalTo(self.view)
            make.top.equalTo(profilePictureImageView.snp.bottom).offset(10)
        }
        
        tableView.snp.makeConstraints { make in
            make.left.right.bottom.equalTo(self.view)
            make.top.equalTo(usernameLabel.snp.bottom).offset(20)
        }
    }
}

extension UserProfileViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UserProfileSectionHeaderView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 30))
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}

extension UserProfileViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return musicGenres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MoreGenresCell
        cell.genreImageView.image = colorImages[indexPath.row]
        cell.nameLabel.text = musicGenres[indexPath.row]
        cell.pointsLabel.text = "111"
        cell.selectedImageView.isHidden = true
        cell.pointsLabel.isHidden = false
        return cell
    }
    
    
}
