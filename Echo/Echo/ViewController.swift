//
//  ViewController.swift
//  Echo
//
//  Created by Pero on 5/22/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit
import SnapKit
import AVFoundation
import AVKit
import Social


class ViewController: UIViewController {
    
    var imagePicker = UIImagePickerController()
    var logoImageView : UIImageView!
    var welcomeLabel : UILabel!
    var descriptionLabel : UILabel!
    var nextButton : UIButton!
    var videoImageView : UIImageView!
    var videoButton : UIButton!
    let playerController = AVPlayerViewController()
    let swiftColor = UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 1).cgColor
    var shareButton : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setupView() {
        UserDefaults.standard.set(0, forKey: "activationFinish")
        UserDefaults.standard.set(0, forKey: "points")
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = UIColor.white
        logoImageView = UIImageView()
        logoImageView.image = #imageLiteral(resourceName: "logoAppIcon")
        logoImageView.layer.masksToBounds = true
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.layer.cornerRadius = 10
        
        welcomeLabel = UILabel()
        welcomeLabel.text = "Welcome to Echo"
       
        welcomeLabel.font = MyVariables.fontRegular30
        welcomeLabel.textAlignment = .center
        welcomeLabel.textColor = UIColor.black
        
        descriptionLabel = UILabel()
        if UIDevice.current.modelName.contains("5") || UIDevice.current.modelName.contains("4") {
            descriptionLabel.font = MyVariables.fontBold15
        } else if UIDevice.current.modelName.contains("6") || UIDevice.current.modelName.contains("7") {
            descriptionLabel.font = MyVariables.fontRegular20
        }
        
        descriptionLabel.text = "Check your music knowledge"
        descriptionLabel.textAlignment = .center
        descriptionLabel.textColor = UIColor.black
        
        shareButton = UIButton()
        shareButton.setTitle("Share", for: .normal)
        shareButton.addTarget(self, action: #selector(share), for: .touchUpInside)
        shareButton.backgroundColor = MyVariables.pinkColor
        shareButton.isHidden = true
        
        let path = Bundle.main.path(forResource: "Evolution of Music - Pentatonix", ofType:"mp4")
        let player = AVPlayer(url: URL(fileURLWithPath: path!))
        
        playerController.player = player
        self.addChildViewController(playerController)
        
        videoImageView = UIImageView()
        videoImageView.image = #imageLiteral(resourceName: "Music_First")
        videoImageView.layer.masksToBounds = true
        videoImageView.contentMode = .scaleAspectFill
       
        videoButton = UIButton()
        videoButton.setImage(#imageLiteral(resourceName: "Circled Play-50 (3)"), for: .normal)
        videoButton.imageView?.contentMode = .scaleAspectFit

        videoButton.addTarget(self, action: #selector(click), for: .touchUpInside)
        
        nextButton = UIButton()
        nextButton.setTitle("Next", for: .normal)
        nextButton.setTitleColor(UIColor.white, for: .normal)
        nextButton.layer.backgroundColor = swiftColor
        nextButton.titleLabel?.font = UIFont(name: "SanFranciscoDisplay-Bold", size: 20)
        nextButton.titleLabel?.textAlignment = .center
        nextButton.layer.cornerRadius = 15
        nextButton.addTarget(self, action: #selector(clickNextButton), for: .touchUpInside)
        
        self.view.addSubview(logoImageView)
        self.view.addSubview(welcomeLabel)
        self.view.addSubview(descriptionLabel)
        self.view.addSubview(nextButton)
        self.view.addSubview(playerController.view)
        self.view.addSubview(videoImageView)
        self.view.addSubview(videoButton)
        self.view.addSubview(shareButton)
    }
    
    func setupConstraints() {
        let x = self.view.frame.width/5
        shareButton.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(50)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(100)
            make.height.equalTo(40)
        }

        logoImageView.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(120)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(self.view.snp.width).dividedBy(4)
            make.height.equalTo(x)
        }
        welcomeLabel.snp.makeConstraints { make in
            make.top.equalTo(logoImageView.snp.bottom).offset(10)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(self.view.snp.width).dividedBy(1.2)
            make.height.equalTo(self.view.snp.height).dividedBy(15)
        }
        descriptionLabel.snp.makeConstraints { make in
            make.top.equalTo(welcomeLabel.snp.bottom)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(welcomeLabel)
            make.height.equalTo(welcomeLabel)
        
        }
        videoImageView.snp.makeConstraints { make in
            make.top.equalTo(descriptionLabel.snp.bottom).offset(30)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(self.view.snp.width).dividedBy(1.1)
            //make.bottom.equalTo(nextButton.snp.top).offset(-80)
            make.height.equalTo(self.view.snp.height).dividedBy(3.5)
        }
        playerController.view.snp.makeConstraints {make in
            make.edges.equalTo(videoImageView)
        }
        videoButton.snp.makeConstraints { make in
            make.center.equalTo(playerController.view.snp.center)
            make.width.equalTo(self.view.snp.width).dividedBy(5)
            make.height.equalTo(self.view.snp.width).dividedBy(10)
        
        }
        nextButton.snp.makeConstraints { make in
            make.bottom.equalTo(self.view).offset(-30)
            make.centerX.equalTo(self.view.center)
            make.width.equalTo(self.view.snp.width).dividedBy(1.1)
            make.height.equalTo(self.view.snp.height).dividedBy(10)
        }
    }
    
    
    func share() {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
            let fbShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
           // fbShare.add(videoImageView.image)
            let url = URL(string: "http://www.dailymotion.com/video/x5r14dw_portugal-u21-serbia-u21-2-0-highlights-euro-u21_sport")
            fbShare.add(url)
            self.present(fbShare, animated: true, completion: nil)
            
        } else {
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }

    }

    
    func click() {
        playVideo()
        videoImageView.isHidden = true
        videoButton.isHidden = true
    }
    
    func clickNextButton() {
        playerController.player?.pause()
        self.navigationController?.pushViewController(SignUpViewContoller(), animated: true)
        
    }
    
    func playVideo(){
        playerController.player?.play()
    }
}
