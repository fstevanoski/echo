//
//  WrongAnswerViewController.swift
//  Echo
//
//  Created by Pero on 6/12/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UIKit

class WrongAnswerViewController: UIViewController {

    var artistImageView : UIImageView!
    //var label : UILabel!
    var okButton : UIButton!
    var tableView : UITableView!
    var items = ["Play this song","AC/DC Accessories","Buy tickets for next concerts"]
    var images = [#imageLiteral(resourceName: "music_note"),#imageLiteral(resourceName: "shoppingCart"),#imageLiteral(resourceName: "tickets")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addPoints()
        setupView()
        setupConstraints()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.isTranslucent = true
        
    }
    
    func setupView() {
        navigationItem.hidesBackButton = true
        self.view.backgroundColor = UIColor.white
        self.navigationItem.prompt = "Wrong Answer"
        self.title = "-5 Points"
        
        artistImageView = UIImageView()
        let songNumb = UserDefaults.standard.integer(forKey: "showSong")
        switch songNumb {
        case 1:
            artistImageView.image = #imageLiteral(resourceName: "acdc_new1")
        case 2:
            artistImageView.image = #imageLiteral(resourceName: "despasitoNotification.jpg")
        case 3:
            artistImageView.image = #imageLiteral(resourceName: "beethovenNot.jpg")
        case 4:
            artistImageView.image = #imageLiteral(resourceName: "chuckNotification.jpg")
        default:
            break
        }

        artistImageView.contentMode = .scaleAspectFit
        
//        label = UILabel()
//        label.text = "Better Luck Next Time"
//        label.font = UIFont.systemFont(ofSize: 20)
//        label.textAlignment = .center
//        label.textColor = UIColor.black
        
        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 70, bottom: 0, right: 0)
        tableView.register(CorrectAnswerTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView()
        
        okButton = UIButton()
        okButton.setTitle("OK", for: .normal)
        okButton.setTitleColor(UIColor.white, for: .normal)
        okButton.titleLabel?.font = MyVariables.fontRegular20
        okButton.backgroundColor = MyVariables.pinkColor
        okButton.layer.cornerRadius = 15
        okButton.addTarget(self, action: #selector(okButtonTouched), for: .touchUpInside)
        
        self.view.addSubview(artistImageView)
        self.view.addSubview(tableView)
        self.view.addSubview(okButton)
    }
    
    func setupConstraints() {

        artistImageView.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(110)
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
            make.height.equalTo(self.view.frame.size.width-60)
        }
        tableView.snp.makeConstraints { make in
            make.top.equalTo(artistImageView.snp.bottom).offset(10)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(okButton.snp.top)
        }
        
        okButton.snp.makeConstraints { make in
            make.bottom.equalTo(self.view).offset(-10)
            make.left.equalTo(self.view).offset(10)
            make.right.equalTo(self.view).offset(-10)
            make.height.equalTo(self.view.snp.height).dividedBy(10)
        }
    }
    
    func addPoints() {
        var points = UserDefaults.standard.value(forKey: "points") as! Int
        points -= 5
        UserDefaults.standard.set(points, forKey: "points")
    }
    
    func okButtonTouched() {
       _ = self.navigationController?.popViewController(animated: true)
    }
}

extension WrongAnswerViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == 0 {
//            navigationController?.pushViewController(PlaySongViewController(), animated: true)
//        }
    }
}

extension WrongAnswerViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CorrectAnswerTableViewCell
        cell.setupCell(image: images[indexPath.row], text: items[indexPath.row])
        
        return cell
    }
}
