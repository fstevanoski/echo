//
//  NotificationService.swift
//  NotificationService
//
//  Created by Pero on 6/12/17.
//  Copyright © 2017 shortway. All rights reserved.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {
    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        // Get the custom data from the notification payload
        if let notificationData = request.content.userInfo["data"] as? [String: String] {
            
            
            if let urlString = notificationData["attachment-url"], let fileUrl = URL(string: urlString) {
                // Download the attachment
                URLSession.shared.downloadTask(with: fileUrl) { (location, response, error) in
                    if let location = location {
                        // Move temporary file to remove .tmp extension
                        let tmpDirectory = NSTemporaryDirectory()
                        let tmpFile = "file://".appending(tmpDirectory).appending(fileUrl.lastPathComponent)
                        let tmpUrl = URL(string: tmpFile)!
                        try! FileManager.default.moveItem(at: location, to: tmpUrl)
                        
                        // Add the attachment to the notification content
                        if let attachment = try? UNNotificationAttachment(identifier: "", url: tmpUrl) {
                            self.bestAttemptContent?.attachments = [attachment]
                            
                        }
                    }
                    let aps = request.content.userInfo["aps"] as! [String : Any]
                    let song1 = aps["song1"] as! String
                    let song2 = aps["song2"] as! String
                    let song3 = aps["song3"] as! String
                    if #available(iOS 10.0, *) {
                        
                        let song1_1 = UNNotificationAction(
                            identifier: "song1",
                            title: song1,
                            options: .foreground
                        )
                        let song1_2 = UNNotificationAction(
                            identifier: "song2",
                            title: song2,
                            options: .foreground
                        )
                        let song1_3 = UNNotificationAction(
                            identifier: "song3",
                            title: song3,
                            options: .foreground
                        )
                        
                        let category1 = UNNotificationCategory(
                            identifier: "test1",
                            actions: [song1_1,song1_2,song1_3],
                            intentIdentifiers: []
                        )
                        UNUserNotificationCenter.current().setNotificationCategories([category1])
                    }
                    // Serve the notification content
                    self.contentHandler!(self.bestAttemptContent!)
                    }.resume()
            }
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
}
